package com.backend;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.models.Tag;

public class TagManager extends DBObject {
	
	public TagManager() {
		/* null initializer*/
	}
	//t1 = tagQuizRelationsTabl t2 = tagTable
	private String joinBase= "SELECT t1.id, t1.quiz_id, t2.user_id, t2.tag FROM " 
			+ DBObject.tagTable + " As t2 INNER JOIN " + DBObject.tagQuizRelationsTable 
			+ " As t1 ON t1.tag_id = t2.id"; 
	
	private String fetchUserString = joinBase + " WHERE t2.user_id = ? LIMIT 0,30";
	

	private String fetchQuizString = joinBase + " WHERE t1.quiz_id = ? LIMIT 0,30";

	/**
	 * Given a userId, return a list of tags the user created.
	 * @param userid
	 * @return null if the fetch was not successful
	 */
	public List<Tag> fetchTagsForUser(int userid) {
		if(!this.conPrepare(fetchUserString)) return null;
		ResultSet r;
		List<Tag>result;
		try {
			prepStatement.setInt(1, userid);
			r = prepStatement.executeQuery();
			result = new ArrayList<Tag>();
			while (r.next()) {
					result.add(new Tag(r.getInt("id"),
									r.getInt("quiz_id"),
									r.getInt("user_id"), 
									r.getString("tag")
									));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return result;
	}
	/**
	 * 
	 * Given a quiz_id, return a list of tags the user created.
	 * @param userid
	 * @return null if the fetch was not successful
	 */
	public List<Tag> fetchTagsForQuiz(int quizid) {
		if(!this.conPrepare(fetchQuizString)) return null;
		ResultSet r;
		List<Tag>result;
		try {
			prepStatement.setInt(1, quizid);
			r = prepStatement.executeQuery();
			result = new ArrayList<Tag>();
			while (r.next()) {
				result.add(new Tag(r.getInt("id"),
						r.getInt("quiz_id"),
						r.getInt("user_id"), 
						r.getString("tag")
						));
		     }
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return result;
	}
}
