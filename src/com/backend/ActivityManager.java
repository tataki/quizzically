package com.backend;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.backend.DBObject;
import com.models.Activity;
import com.models.Quiz;
import com.models.User; // only for getting recent friend activities

public class ActivityManager extends DBObject {
	
	String sorted_score_desc = " ORDER BY score DESC ";
	String sorted_score_timestamp_desc = " ORDER BY score DESC, timestamp DESC ";
	
	
	/*
	 * Methods for retrieving list of activity:
	 *  - by userid
	 *  - by most recent overall
	 *  - on a given day
	 *  - sorted by top performing
	 * 
	 */
	public ActivityManager() {
		/* empty initializer */
	}
	
	private List<Integer> findQuestionMarkIndices(String str) {
		List<Integer> indices = new ArrayList<Integer>();
		int lastIndex = 0;
		int last = 0;
		while(true) {
			int idx = insert.indexOf('?', last);
			if(idx == -1) break;
			indices.add(idx);
			last = insert.indexOf('?', last);
			lastIndex++;
		}
		return indices;
	}
	
	public String insert = "INSERT INTO " + DBObject.activityTable + 
							" (user_id, quiz_id, score, timeTaken) VALUES (?, ?, ?)";
	public boolean addActivities(List<Activity> activities) {
		List<String>queries = new ArrayList<String>();
		List<Integer> indices = findQuestionMarkIndices(insert);
		StringBuilder s;
		for(Activity a : activities) {
			s = new StringBuilder(insert);
			s.replace(indices.get(0), indices.get(0), "" + a.getUser_id());
			s.replace(indices.get(1), indices.get(1), "" + a.getQuiz_id());
			s.replace(indices.get(2), indices.get(2), "" + a.getScore());
		}
		return (this.executeBatch(queries) > 0);
	}
	
	private String base = "SELECT * FROM " + DBObject.activityTable;
	/**Tested by Chris
	 * Primary retrieval method; converts ResultSet to ordered
	 * list of Activities. Select columns from base string must match
	 * the ones being parsed here.
	 * 
	 * Returns an empty list if no result is found for this query.
	 * 
	 */
	private List<Activity>convertToList(ResultSet r) throws SQLException {
		if(r == null) return null;
		List<Activity> result = new ArrayList<Activity>();
		while(r.next()) {
			result.add(new Activity(r.getInt("id"),
									r.getInt("user_id"), 
									r.getInt("quiz_id"),
									r.getDouble("score"),
									r.getTimestamp("timestamp"),
									r.getDouble("timeTaken")
								   ));
		}
		return result;
	}
	
	boolean debug = false;
	// nDEBUG
	
	/**TODO: NOT TESTED
	 * specific - gets Activity for this user
	 */
	public List<Activity>getbyUserId(int userid) {
		String filter = " WHERE user_id = ? ";
		if(!this.conPrepare(base + filter + limit)) return null;
		try {
			// where user_id = userid
			prepStatement.setInt(1, userid);
			ResultSet r = this.getPrepared();
			
			return convertToList(r);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/** TODO: NOT TESTED
	 * generic - gets recent activity across all sites
	 * @return
	 */
	public List<Activity>getRecent() {
		if(!this.conPrepare(base + recent + limit)) return null;
		try {
			if(debug) System.out.println(prepStatement.toString());
			ResultSet r = this.getPrepared();
			return convertToList(r);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/** TODO: NOT TESTED
	 * gets all activity for today
	 * @return
	 */
	public List<Activity>getToday() {
		if(!this.conPrepare(base + today + limit)) return null;
		try {
			ResultSet r = this.getPrepared();
			return convertToList(r);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/** TODO: NOT TESTED
	 * gets a specific column of user scores. 
	 * Uses the "extended" constructor for an activities
	 * class, which takes in an extra username field.
	 * Makes it very convenient to get a list activites,
	 * scores, and usernames ready for display
	 * 
	 *  query: 
	 *  select * from Quiz_activities where quiz_id = quizid order by score desc
	 *  	INNER JOIN Quiz_activites ON
	 *  	Quiz_user.username where id = Quiz_activities.user_id 
	 */
	private String topScore_complex_query = "select * from Quiz_activities where quiz_id = ? order by score desc " +
			" INNER JOIN " + DBObject.activityTable +" ON " + "Quiz_user.username " +
			" WHERE id = Quiz_activities.user_id " + limit;
	
	public List<Activity> getTopScores(int quizid) {
		if(! this.conPrepare(topScore_complex_query)) return null;
		try {
			// where user_id = userid
			prepStatement.setString(1, "quiz_id");
			prepStatement.setInt(2, quizid);
			prepStatement.setString(3,"score");
			ResultSet r = prepStatement.executeQuery();
			List<Activity> result = new ArrayList<Activity>();
			while(r.next()) {
				result.add(new Activity(r.getInt("id"),
										r.getInt("user_id"), 
										r.getInt("quiz_id"),
										r.getInt("score"),
										r.getTimestamp("timestamp"),
										r.getDouble("timeTaken"),
										r.getString("username")
									   ));
			}
			return result;
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/** Tested by Chris
	 * gets a specific column of user scores. 
	 * Uses the "extended" constructor for an activities
	 * class, which takes in an extra username field.
	 * Makes it very convenient to get a list activites,
	 * scores, and usernames ready for display
	 * 
	 *  query: 
	 *  select * from Quiz_activities where quiz_id = quizid order by score desc
	 *  	INNER JOIN Quiz_activites ON
	 *  	Quiz_user.username where id = Quiz_activities.user_id 
	 */
	private String topScoreQuery = "SELECT score FROM Quiz_activity WHERE quiz_id = ? ORDER BY score DESC";
	
	public double getTopScoreByQuizId(int quizid) {
		if(!this.conPrepare(topScoreQuery)) return 0;
		try {
			prepStatement.setInt(1, quizid);
			ResultSet r = prepStatement.executeQuery();
			if (r.next()) {
				return r.getDouble("score");
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	
	/**
	 * Average score query: uses SQL to calculate "averages"
	 */
	private String averageScore = "select AVG(score) from " + DBObject.activityTable;
	
	/**
	 * returns average score for a given user
	 * @param quizid
	 * @return
	 */
	public double getAverageScoreUser(int userid) {
		String filter = " where user_id = ? ";
		if(! this.conPrepare(averageScore + filter)) return 0;
		try {
			// where user_id = userid
			prepStatement.setInt(1, userid);
			ResultSet r = prepStatement.executeQuery();
			System.out.println(prepStatement.toString());
			if(r.next()) {
				return r.getDouble(1);
			}
			return 0;
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * returns average score for a given quiz
	 * @param quizid
	 * @return
	 */
	public double getAverageScoreQuiz(int quizid) {
		String filter = " where quiz_id = ? ";
		if(! this.conPrepare(averageScore + filter)) return 0;
		try {
			// where quiz_id = quizid;
			prepStatement.setInt(1, quizid);
			ResultSet r = prepStatement.executeQuery();
			if(r.next()) {
				return r.getDouble(1);
			}
			return 0;
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	
	/**TODO: NOT TESTED
	 * generic - gets top activity across whole site
	 */
	public List<Activity>getTopActivity() {
		if(!this.conPrepare(base + sorted_score_desc + limit)) return null;
		try {
			if(debug) System.out.println(prepStatement.toString());
			ResultSet r = this.getPrepared();
			return convertToList(r);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**Tested by Chris
	 * specific - gets top activity for this quiz
	 * @param userid
	 * @return
	 */
	public List<Activity> getTopActivityForQuiz(int quizid) {
		String filter = " WHERE quiz_id = ? ";
		if(!this.conPrepare(base + filter + sorted_score_timestamp_desc)) return null;
		try {
			prepStatement.setInt(1, quizid);
			ResultSet r = this.getPrepared();
			List<Activity> result = new ArrayList<Activity>();
			double highestScore = 0;
			if (r.next())
				highestScore = r.getDouble("score");
			r.beforeFirst();
			while (r.next()) {
				if (r.getDouble("score") == highestScore) {
					result.add(new Activity(r.getInt("id"),
											r.getInt("user_id"), 
											r.getInt("quiz_id"),
											r.getDouble("score"),
											r.getTimestamp("timestamp"),
											r.getDouble("timeTaken")
										   ));
				}
				else
					return result;
			}
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**Tested by Chris
	 * specific - gets top activity for this user
	 * @param userid
	 * @return
	 */
	public List<Activity>getTopActivityForUser(int userid) {
		String filter = " WHERE user_id = ? ";
		if(!this.conPrepare(base + filter + sorted_score_desc + limit)) return null;
		try {
			// set the filter of user_id to useid
			prepStatement.setInt(1, userid);
			ResultSet r = this.getPrepared();
			return convertToList(r);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**Tested by Chris
	 * specific - gets recent activity for this user
	 * @param userid
	 * @return
	 */
	public List<Activity>getRecentActivity(int userid) {
		String filter = " WHERE user_id = ? ";
		if(!this.conPrepare(base + filter + recent + limit)) return null;
		try {
			// set the filter of user_id to useid
			prepStatement.setInt(1, userid);
			ResultSet r = this.getPrepared();
			return convertToList(r);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**Tested by Chris
	 * specific - gets recent activity for this quiz
	 * @param quizid
	 * @return
	 */
	public List<Activity>getRecentActivityByQuizId(int quizid) {
		String filter = " WHERE quiz_id = ? ";
		if(!this.conPrepare(base + filter + recent + limit)) return null;
		try {
			prepStatement.setInt(1, quizid);
			ResultSet r = this.getPrepared();
			return convertToList(r);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**Tested by Chris
	 * specific - gets recent activity for this quiz from the last day
	 * @param quizid
	 * @return
	 */
	public List<Activity> getRecentActivityByQuizIdLastDay(int quizid) {
		String filter = " AND quiz_id = ? ";
		if(!this.conPrepare(base + today + filter + recent + limit)) return null;
		try {
			prepStatement.setInt(1, quizid);
			ResultSet r = this.getPrepared();
			return convertToList(r);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	/**Tested by Chris
	 * specific - gets recent activity for a specific user and quiz
	 * @param userid
	 * @return
	 */
	public List<Activity> getRecentActivityForUserAndQuiz(int userid, int quizid) {
		String filter = " WHERE user_id = ? AND quiz_id = ? ";
		if(!this.conPrepare(base + filter + recent + limit)) return null;
		try {
			prepStatement.setInt(1, userid);
			prepStatement.setInt(2, quizid);
			ResultSet r = this.getPrepared();
			return convertToList(r);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/** 
	 * specific - get friends' top activity specific to this user
	 */
	// parts copied from UserManager.java ; not shared
	private String recentFriendActivity_complex_string =
		base + 
		" WHERE user_id IN " + 
		" (SELECT user1_id from " + DBObject.friendshipTable + " WHERE friendType = 3 AND user2_id = ? " +
		" UNION" +
		" SELECT user2_id from " + DBObject.friendshipTable + " WHERE friendType = 3 AND user1_id = ?) " +
		recent;
	
	public List<Activity>getRecentFriendActivity(int userid, List<User>friends) {
		
		if(!this.conPrepare(recentFriendActivity_complex_string)) return null;
		try {
			// sorted_desc requires a column name
			prepStatement.setInt(1, userid);
			prepStatement.setInt(2, userid);
			if(debug) System.out.println(prepStatement.toString());
			ResultSet r = this.getPrepared();
			return convertToList(r);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * gets number of activities that this user has recorded in the database
	 */
	private String getTotalActivityCount = "select count(*) from " + DBObject.activityTable + 
						" where user_id = ? ";
	public int getActivityCountForUser(int userid) {
		if(!this.conPrepare(getTotalActivityCount)) return 0;
		try {
			// sorted_desc requires a column name
			prepStatement.setInt(1, userid);
			System.out.println(prepStatement.toString());
			ResultSet r = prepStatement.executeQuery();
			if(r.next()) {
				return r.getInt(1);
			}
			return 0;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/** Tested by Chris
	 * Get number of quizzes user has taken.
	 * @param userid
	 * @return numQuizzes
	 */
	public int getQuizCountForUser(int userid) {
		String filter = " WHERE user_id = ? GROUP BY quiz_id";
		int count = 0;
		if(!this.conPrepare(base + filter)) return 0;
		try {
			// sorted_desc requires a column name
			prepStatement.setInt(1, userid);
			ResultSet r = this.getPrepared();
			while (r.next())
				count++;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
		return count;
	}
	
	/** Tested by Chris
	 * Get number of times a quiz has been taken.
	 * @param quizid
	 * @return count
	 */
	public int getQuizCountTaken(int quizid) {
		String filter = " WHERE quiz_id = ?";
		int count = 0;
		if(!this.conPrepare(base + filter)) return 0;
		try {
			// sorted_desc requires a column name
			prepStatement.setInt(1, quizid);
			ResultSet r = this.getPrepared();
			while (r.next())
				count++;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
		return count;
	}
}
