package com.backend;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.print.attribute.IntegerSyntax;


// import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
/*
	+------------+---------+------+-----+---------+----------------+
	| Field      | Type    | Null | Key | Default | Extra          |
	+------------+---------+------+-----+---------+----------------+
	| id         | int(11) | NO   | PRI | NULL    | auto_increment |
	| user1_id   | int(11) | NO   | MUL | NULL    |                |
	| user2_id   | int(11) | NO   | MUL | NULL    |                |
	| friendType | int(11) | NO   |     | NULL    |                |
	+------------+---------+------+-----+---------+----------------+
 */
/**
 * 
 * @author Sydney
 * This is a simple recommendation system based on the number of shared friends
 * between two users. 
 */
public class FriendRecommendation extends DBObject{
	/*
	 * FriendsTable: 
	 * key: user id
	 * value: friends' ids
	 */
	private HashMap<Integer,HashSet<Integer>> friendsTable;
	private HashMap<FriendPair,Integer>	friendsRecTable;
	private HashSet<FriendPair> noFriendPairs;
	private int NUM_RECOMMENDATIONS = 30;
	private String base = "";
	private String updateRec = "UPDATE " + DBObject.friendRecommendationTable + " SET numFriends = ? WHERE user1_id = ? AND user2_id = ?";
	private String decrementNumFriends = "UPDATE " + DBObject.friendRecommendationTable + " SET numFriends = numFriends-1 WHERE user1_id = ? AND user2_id = ?";
	
	private String insertRec = "INSERT INTO " + DBObject.friendRecommendationTable + 
									" VALUE (null,?,?,?)";
	private String incrementNumFriends = "UPDATE " + DBObject.friendRecommendationTable + " SET numFriends = numFriends+1 WHERE user1_id = ? AND user2_id = ?";

	private String deleteRec = "DELETE FROM " + DBObject.friendRecommendationTable + 
	" WHERE (user1_id = ? AND user2_id = ?)";
	private String deleteAllRecByIdQuery = "DELETE FROM " + DBObject.friendRecommendationTable + " WHERE user1_id = ? OR user2_id = ?";
	private String selectNumFriendsQuery = "SELECT numFriends FROM " + DBObject.friendRecommendationTable + " WHERE user1_id = ? AND user2_id = ?";
	private static boolean isRecTableSet = false;
	public class FriendPair extends HashSet<Integer>{
		
		FriendPair(int user1, int user2){
			super.add(user1);
			super.add(user2);
		
		}
		public Integer[] getUsers(){
			Integer[] pairArray	= this.toArray(new Integer[0]);
			if(pairArray[0]>pairArray[1]){
				int temp = pairArray[0];
				pairArray[0] = pairArray[1];
				pairArray[1] = temp;
			}
			return pairArray;
		}
		
		public String toString(){
			return this.getUsers().toString();
		}
		
	}
	
	public FriendRecommendation(){
		super();
		
	}
	//find number of common friend between 2 users.
	public int getNumFriends(int user1, int user2){
		if(user1>user2){
			int temp = user1;
			user1 = user2;
			user2 = temp;
		}
		
		if(!conPrepare(selectNumFriendsQuery)) return 0;

		try {
			prepStatement.setInt(1, user1);
			prepStatement.setInt(2, user2);
			ResultSet rs = prepStatement.executeQuery();
			if(!rs.next()) return 0;
			return rs.getInt("numFriends");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}
	/**
	 * Usage:
	 * If the number of user > 10
	 * setFriendRecommendationTable();
	 *		
	 * 
	 * Setup the FriendRecommendation Table based on friendship table.
	 * This should be called only once for initiating the state the recommendation table. 
	 * @return true if the FriendRecommendationTable is set up correctly.
	 */
	public boolean setFriendRecommendationTable(){
		friendsTable = new HashMap<Integer,HashSet<Integer>>();	
		noFriendPairs =  new HashSet<FriendPair>();
		friendsRecTable = new HashMap<FriendPair,Integer>();
		setFriendsTable();
		SetFriendRecTable();
		//*upload to Quiz_friendShip
	
		try {
			for(Entry<FriendPair, Integer> entry:friendsRecTable.entrySet()){
				//if exsit update
				if(! this.conPrepare(updateRec)) return false;
				Integer[] pairArray = entry.getKey().getUsers();	
				prepStatement.setInt(1,entry.getValue());
				prepStatement.setInt(2,pairArray[0]);
				prepStatement.setInt(3,pairArray[1]);
				int effectedRow = prepStatement.executeUpdate();
				if(effectedRow==0){
					if(! this.conPrepare(insertRec)) return false;
					prepStatement.setInt(1,pairArray[0]);
					prepStatement.setInt(2,pairArray[1]);
					prepStatement.setInt(3,entry.getValue());
					prepStatement.execute();
						
				}
			}	
		} catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;	
	}
	/**
	 * Delete is only called when user_id is removed from database.
	 * @param user_id
	 * @return
	 */
	public boolean delete(int user_id){
		if(!conPrepare(deleteAllRecByIdQuery)) return false;
		try {
			prepStatement.setInt(1, user_id);
			prepStatement.setInt(2, user_id);
			prepStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	private boolean delete(int user1, int user2){
		try {
			
			if(! this.conPrepare(deleteRec)) return false;
			
			prepStatement.setInt(1,user1);
			prepStatement.setInt(2,user2);
			prepStatement.execute();
				
		} catch(SQLException e) {
			e.printStackTrace();
		}		
		
		return true;
	}
	
	private boolean decrementNumFriends(int user1, int user2){
		try {
			ResultSet rs = getResults("SELECT numFriends FROM " + DBObject.friendRecommendationTable + " WHERE user1_id = " + user1 + "AND user1_id = " + user2);
			rs.next();
		if(rs.getInt("numFriends")!=1){
				if(! this.conPrepare(decrementNumFriends)) return false;			
				prepStatement.setInt(1,user1);
				prepStatement.setInt(2,user2);
				prepStatement.executeUpdate();
				
			}else{
				delete(user1,user2);
			}
				
		} catch(SQLException e) {
			e.printStackTrace();
		}		
		
		return true;
	}
	/**
	 * usage:
	 * insertFriendship(a,b,3);
	 * deleteRecommendation(a,b);
	 * This method is called when a friended b, we want to
	 * remove the corresponding recommendation from the table and update
	 * the statistics.
	 * 
	 * @param friendpair
	 */
	public boolean deleteRecommendation(int user1, int user2){		
		//remove recommendation (a,b) 
		if(user1>user2){
			int temp = user1;
			user1 = user2;
			user2 = temp;
		}
		delete(user1,user2);
		FriendManager fm = new FriendManager();
		List<Integer> user1Friends = fm.getFriendsId(user1);
		List<Integer> user2Friends = fm.getFriendsId(user2);
		// to delete
		Set<Integer> user1FriendsSet = new HashSet<Integer>(user1Friends);
		Set<Integer> user2FriendsSet = new HashSet<Integer>(user2Friends);
		Set<Integer> tempSet = new HashSet<Integer>(user2Friends);
		user2FriendsSet.removeAll(user1FriendsSet);
		user1FriendsSet.removeAll(tempSet);
		//add recommendation (a,c) c is a friend of b
		for(int toAdd: user2FriendsSet){
			if(user1 != toAdd)
				incrementNumFriends(user1, toAdd);
		}
		//add recommend b's friends to a(but not a's friends)
		for(int toAdd: user1FriendsSet){
			if(user2 != toAdd)
				incrementNumFriends(toAdd, user2);
		}
			
		return true;
		
	}
	
	
	private boolean insert(int user1,int user2){
		if(user1>user2){
			int temp = user1;
			user1 = user2;
			user2 = temp;
		}
		if(! this.conPrepare(insertRec)) return false;
		try {
			prepStatement.setInt(1,user1);
			prepStatement.setInt(2,user2);
			prepStatement.setInt(3,1);
			prepStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private boolean incrementNumFriends(int user1, int user2){
		try {	
			if(! this.conPrepare(incrementNumFriends)) return false;			
			prepStatement.setInt(1,user1);
			prepStatement.setInt(2,user2);
			if(prepStatement.executeUpdate()==0){				
				insert(user1,user2);
			}
				
		} catch(SQLException e) {
			e.printStackTrace();
			return false;
		}		
		
		return true;
	}
	
	// for the case where a friendship is deleted.
	// we don't support that ppl can delete friendship right now
	/*
	public void insertRecommendation(int user1, int user2){
		if(user1>user2){
			int temp = user1;
			user1 = user2;
			user2 = temp;
		}
		// insert recomendation for user1<->user2
		insert(user1,user2);
		FriendManager fm = new FriendManager();
		List<Integer> user1Friends = fm.getFriendsId(user1);
		List<Integer> user2Friends = fm.getFriendsId(user2);
		//toAdd
		Set<Integer> user1FriendsSet = new HashSet<Integer>(user1Friends);
		Set<Integer> user2FriendsSet = new HashSet<Integer>(user2Friends);
		Set<Integer> tempSet = new HashSet<Integer>(user2Friends);
		
		user2FriendsSet.removeAll(user1FriendsSet);
		user1FriendsSet.removeAll(tempSet);
		//delete recommend a's friends to b(but not b's friends),
		for(int toDelete: user2FriendsSet){
			decrementNumFriends(user1, toDelete);
		}
		//delete recommend b's friends to a(but not a's friends)
		for(int toDelete: user1FriendsSet){
			decrementNumFriends(toDelete, user2);
		}

		


	}*/
	
	/**
	 * SetFriendTable should be only called once
	 * 
	 * it will fill the friendsTable based on current Friendship Table.
	 */
	private void setFriendsTable(){
		//get user table from DB
		isRecTableSet = false;
		FriendManager fm = new FriendManager();
		HashSet<Integer> friendsSet;
		List<FriendPair> friendships = fm.getAllFriendship();
		for(FriendPair pair : friendships){
			Integer[] pairArray =  pair.getUsers();
			
			int from = pairArray[0];
			int to = pairArray[1];
			noFriendPairs.add(pair);
			if(friendsTable.containsKey(from)){
				friendsSet = friendsTable.get(from);
				friendsSet.add(to);	
			}else{
				friendsSet = new HashSet<Integer>();
				friendsSet.add(to);
				friendsTable.put(from, friendsSet);
			}
			if(friendsTable.containsKey(to)){
				friendsSet = friendsTable.get(to);
				friendsSet.add(from);	
			}else{
				friendsSet = new HashSet<Integer>();
				friendsSet.add(from);
				friendsTable.put(to, friendsSet);
			}
				
		}
			
		
	}
	/**
	 * Finds all the combinations in pairs of a given list.
	 * 
	 */
	private ArrayList<FriendPair> FindTuples(HashSet<Integer> users){
		int tempi, tempj;
		ArrayList<FriendPair> combinations = new ArrayList<FriendPair>();
		FriendPair newPair;
		ArrayList<Integer> users_list = new ArrayList<Integer>(users);
		for(int i = 0; i < users_list.size();i++){
			tempi = users_list.get(i);
			for(int j = i+1; j < users_list.size(); j++){
				tempj = users_list.get(j);	
				
				newPair = new FriendPair(tempi,tempj);
				if(!noFriendPairs.contains(newPair))
					combinations.add(newPair);	
			}
				
		}
		return combinations;
		
	}
	/*
	 * Helper function to set up a recommendation Table;
	 */
	private void SetFriendRecTable(){
		for(int key: friendsTable.keySet()){
			ArrayList<FriendPair> toFriendPairs = FindTuples(friendsTable.get(key));
			for(FriendPair pair: toFriendPairs){
				if(friendsRecTable.containsKey(pair)){
					int currentVal = friendsRecTable.get(pair);
					friendsRecTable.put(pair, currentVal+1);
				}else
					friendsRecTable.put(pair,1);
			}	
		}			
	}

	//for debug
	public HashMap<Integer,HashSet<Integer>> getFriendsTable(){
		return friendsTable;
	}
	
	
	
}
