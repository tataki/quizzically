package com.backend;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.models.Category;
import com.models.Quiz;
import com.models.questions.BaseQuestion;
import com.models.questions.QuestionGSON;

/**
 * 
 * @author Sydney
 * QuizManager deals with quiz table & question table
 */
public class QuizManager extends DBObject {
	
	public QuizManager(){
		super();
	}
	
	/** base defines the basic select query to use for this Manager.
	 * convertToList must convert based on the same columns
	 * that base contains to convert ResultSet to List
	 */
	private String base = "SELECT * FROM " + DBObject.quizTable;
	
	private String predicate_sub = " WHERE id IN ";
	private String sorted_rating_desc = " ORDER BY rating DESC ";
	private String creator_id_filter = " WHERE creator_id = ?";
	private String name_filter = " WHERE name LIKE ?";
	/******tag******/
	//t1 : Quiz_quiz
	//t2 : Quiz_tag
	//t3 : Quiz_tagQuiz
	
	
	private String tagJoinedTagQuiz = " (Quiz_tag as t2 INNER JOIN Quiz_tag_quiz as t3 ON t2.id = t3.tag_id) ";
	private String tagJoinedBase = "SELECT  t1.* FROM Quiz_quiz as t1 LEFT JOIN" + tagJoinedTagQuiz +"ON t3.quiz_id = t1.id ";
	private String tagJoinedOrder  = " ORDER BY case when t2.tag LIKE ? then 1 else 0 end + "
									+"case when t1.name LIKE ? then 1 else 0 end + "
									+"case when t1.description LIKE ? then 1 else 0 end DESC";
	private String tagJoinedQuery = tagJoinedBase + " WHERE t2.tag LIKE ? OR t1.name LIKE ? OR t1.description LIKE ?" + tagJoinedOrder;
	 
	private List<Quiz>convertToList(ResultSet r) throws SQLException {
		List<Quiz> result = new ArrayList<Quiz>();
		while(r.next()) {
			result.add(Quiz.fetch(r.getInt("id")));
		}
		return result;
	}
	
	
	/**
	 * Get number of quizzes based on a specified creator id. 
	 */
	public int getNumOfQuizzesByUserId(int creatorId) {
		if(!this.conPrepare(base + creator_id_filter)) return 0;
		int numQuizzes = 0;
		try {
			prepStatement.setInt(1, creatorId);
			ResultSet r = prepStatement.executeQuery();
			while (r.next())
				numQuizzes++;
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return numQuizzes;
	}
	
	/** NOT TESTED
	 * Get list of quizzes based on a specified creator id. 
	 */
	public List<Quiz> getByUserId(int creatorId) {
		if(! this.conPrepare(base + creator_id_filter + limit)) return null;
		try {
			prepStatement.setInt(1, creatorId);
			ResultSet r = prepStatement.executeQuery();
			return convertToList(r);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/** 
	 * tested by Sydney
	 * Gets a list of quizzes whose names are partial
	 * matches with the queryString
	 */
	public List<Quiz> getSimilarQuizzes(String queryString) {
		if(! this.conPrepare(base + name_filter + limit)) return null;
		try {
			// "where name LIKE queryString"
			prepStatement.setString(1, "%"+queryString+"%");
			ResultSet r = prepStatement.executeQuery(); 
			return convertToList(r);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/** 
	 * tested by Sydney
	 * Gets a list of quizzes whose tag, description, and  name are partial
	 * matches with the queryString. It is sorted by their relevance
	 */
	public List<Quiz> getRelevantQuizzes(String filter) {
		if(! this.conPrepare(tagJoinedQuery)) return null;
		try {
			// "where name LIKE queryString"
			prepStatement.setString(1, "%"+filter+"%");
			prepStatement.setString(2, "%"+filter+"%");
			prepStatement.setString(3, "%"+filter+"%");
			prepStatement.setString(4, "%"+filter+"%");
			prepStatement.setString(5, "%"+filter+"%");
			prepStatement.setString(6, "%"+filter+"%");
			ResultSet r = prepStatement.executeQuery(); 
			List<Quiz> duplicatedList = convertToList(r);
			List<Quiz> toReturn = new ArrayList<Quiz>();
			Set<Quiz>  addedSet = new HashSet<Quiz>();
			//clean up the duplicate 
			for(Quiz q : duplicatedList){
				if(!addedSet.contains(q)){
					toReturn.add(q);
					addedSet.add(q);
				}
			}
			return toReturn;
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public String category_subquery = 
		" ( select quiz_id from " + DBObject.categoryTable + name_filter + " ) ";
	/** NOT TESTED
	 * Does a predicate subquery to find all quizzes that 
	 * have a certain category. Uses the fact that categories
	 * have a quiz_id field.
	 * 
	 * query should be:
	 * select (id, name, description, rating) from Quiz_quiz 
	 * 		where id in (select quiz_id from Quiz_category WHERE name='science') limit 30
	 */
	public List<Quiz> getByCategory(String category) {
		
		if(! this.conPrepare(base + predicate_sub + category_subquery + limit)) return null;
		try {
			// see above query
			prepStatement.setString(1, category);
			System.out.println(prepStatement.toString());
			ResultSet r = prepStatement.executeQuery();
			return convertToList(r);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/** Tested by Chris
	 * gets most recently created quizzes 
     * to do so, does order-by-desc query on timestamp*/
	public List<Quiz> getRecent() {
		if(! this.conPrepare(base + recent + limit)) return null;
		try {
			// getRecent doesn't require any parameter substitution
			// "recent" clause orders by TIMESTAMP so make sure that
			// that field name exists for Quiz object; if not this 
			// won't work
			ResultSet r = prepStatement.executeQuery();
			return convertToList(r);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/** Tested by Chris
	 * Gets most popular quizzes by number of people who have taken it.
	 * 
	 * query: "SELECT quiz_id, COUNT(*) FROM Quiz_activity GROUP BY Quiz_id ORDER BY COUNT(*) DESC;"
	 */
	public List<Quiz> getPopular() {
		List<Quiz> quizzes = new ArrayList<Quiz>();
		if(! this.conPrepare("SELECT quiz_id, COUNT(*) FROM Quiz_activity GROUP BY quiz_id ORDER BY COUNT(*) DESC")) return null;
		try {
			ResultSet r = prepStatement.executeQuery();
			while (r.next()) {
				quizzes.add(Quiz.fetch(r.getInt("quiz_id")));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return quizzes;
	}
	
	/** Tested by Chris
	 * Get all categories.
	 * 
	 * Query: "SELECT * FROM Quiz_category;"
	 */
	public List<Category> getAllCategories() {
		StringBuilder query = new StringBuilder();
		List<Category> categories = new ArrayList<Category>();
		query.append("SELECT * FROM " + DBObject.categoryTable);
		ResultSet rs = getResults(query.toString());
		try {
			while (rs.next()) {
				categories.add(new Category(rs.getInt("id")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categories;
	}
	
}
