package com.backend;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.backend.FriendRecommendation.*;
import com.models.User;
/**
 * 
 * @author Sydney
 * FriendManager provides statics methods to communcate with Quiz_FriendShip and Quiz_Recommendation
 */
public class FriendManager extends DBObject {
	private int NUMFRIENDS = 30;
	//t1 represents *Other*Table;
	//t2 represents userTable
	private String base = "SELECT t2.id, t2.username, t2.email, t2.admin FROM " + DBObject.friendshipTable + " as t1";
	private String joinedBase = base + " INNER JOIN " + DBObject.userTable + " as t2";
	/**** Friendship ****/
	private String friendJoinedUserGivenIdQuery = 
			joinedBase + 
			" ON (t1.user2_id = t2.id AND" +
			" t1.user1_id = ? AND t1.friendType = 3)" + 
			"OR (t1.user1_id = t2.id AND" +
			" t1.user2_id = ? AND t1.friendType = 3)";
	private String joinedGivenNameBase = base + " INNER JOIN " + DBObject.userTable + " as t2 ";
	private String friendJoinedUserGivenNameQuery = 
		joinedGivenNameBase + 
		" ON (t1.user2_id = t2.id AND" +
		" t1.user1_id = ? AND t1.friendType = 3)" + 
		" OR (t1.user1_id = t2.id AND" +
		" t1.user2_id = ? AND t1.friendType = 3) " +
		" WHERE t2.username LIKE ? ";
	private static String user1FriendQuery = "SELECT * FROM " + DBObject.friendshipTable + " WHERE user1_id=?";
	private static String user2FriendQuery = "SELECT * FROM " + DBObject.friendshipTable + " WHERE user2_id=?";
	private String allFriendsQuery = "SELECT * FROM " + DBObject.friendshipTable 
		+ " WHERE friendType = 3";
	
	/***** FriendRecommendation *****/
	private String recommendationQuery = "SELECT user1_id, user2_id FROM " + DBObject.friendRecommendationTable + " WHERE user1_id = ? OR user2_id = ? ORDER BY -numFriends LIMIT 0,30";
	private String recBase = "SELECT t2.id, t2.username, t2.email, t2.admin FROM " + DBObject.friendRecommendationTable + " as t1"; 
	private String joinedRecGivenNameBase = recBase + " INNER JOIN " + DBObject.userTable + " as t2 ";

	private String recJoinedUserGivenNameQuery = 
		joinedRecGivenNameBase + 
		" ON (t1.user2_id = t2.id AND" +
		" t1.user1_id = ?)" + 
		" OR (t1.user1_id = t2.id AND" +
		" t1.user2_id = ?)" +
		" WHERE t2.username LIKE ? ORDER BY -t1.numFriends LIMIT 30";


	/**
	 * Gets friends by specified userid
	 *
	 * Here we query from a joined table of users and friends to retrieve users object more efficiently.
	 * 
	 * @param userid
	 * @return
	 */
	public List<User> getFriends(int userId) {
		if(! this.conPrepare(friendJoinedUserGivenIdQuery)) return null;
		try {
			prepStatement.setInt(1, userId);
			prepStatement.setInt(2, userId);
			ResultSet r = prepStatement.executeQuery(); 
			return UserManager.convertToList(r);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Convert a result set to a list of Id given the field in the table.ie. user1_id
	 * @param r
	 * @param userId
	 * @return
	 */
	private static List<Integer> convertToListId(ResultSet r, String userId){
		List<Integer> list = new ArrayList<Integer>();
		try {
			while(r.next()){
				list.add(r.getInt(userId));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * Returns a list of  user's friends' id
	 * @return
	 */
	public  List<Integer> getFriendsId(int userId) {
		List<Integer> friendsId = new ArrayList<Integer>();
		if(! this.conPrepare(user1FriendQuery)) return null;		
		try {
			prepStatement.setInt(1, userId);
			ResultSet r = prepStatement.executeQuery(); 
			friendsId.addAll(convertToListId(r,"user2_id"));
		} catch(SQLException e) {
			e.printStackTrace();
		}
		if(! this.conPrepare(user2FriendQuery)) return null;		
		try {
			prepStatement.setInt(1, userId);
			ResultSet r = prepStatement.executeQuery(); 
			friendsId.addAll(convertToListId(r,"user1_id"));
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return friendsId;
	}
	
	/**
	 * Given a filter, returns all the Friends that matches the filter
	 * @param userId
	 * @param filter
	 * @return
	 */
	public List<User> getFriends(int userId,String filter){
		if(! this.conPrepare(friendJoinedUserGivenNameQuery)) return null;
		try {
			prepStatement.setInt(1, userId);
			prepStatement.setInt(2, userId);
			prepStatement.setString(3, "%"+filter+"%");
			ResultSet r = prepStatement.executeQuery(); 
			return UserManager.convertToList(r);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Given a filter, returns all the recommended users that matches the filter
	 * @param userId
	 * @param filter
	 * @return
	 */
	public List<User> getRecommendation(int userId,String filter){
		if(! this.conPrepare(recJoinedUserGivenNameQuery)) return null;
		try {
			prepStatement.setInt(1, userId);
			prepStatement.setInt(2, userId);
			prepStatement.setString(3, "%"+filter+"%");
			ResultSet r = prepStatement.executeQuery(); 
			return UserManager.convertToList(r);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 *  This is for FriendRecommendation internal use.
	 * @return
	 */
	public List<FriendRecommendation.FriendPair> getAllFriendship(){
		ResultSet rs = getResults(allFriendsQuery);
		List<FriendRecommendation.FriendPair> friendships = new ArrayList<FriendRecommendation.FriendPair>();
		FriendRecommendation x = new FriendRecommendation();
		try {
			while(rs.next()){
				FriendPair newPair = x.new FriendPair(rs.getInt("user1_id"),rs.getInt("user2_id"));
			    friendships.add(newPair);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return friendships;
	}
	/**
	 * Given a userId, returns the recommended users to be friends.
	 * @param userId
	 * @return
	 */
	public List<User> getRecommendation(int userId){
		
		ArrayList<User> recommendations = new ArrayList<User>();
		ArrayList<Integer> recommendationsId = new ArrayList<Integer>();
		//find sorted user ids
		if(! this.conPrepare(recommendationQuery)) return null;
		try {
			prepStatement.setInt(1,userId);
			prepStatement.setInt(2,userId);
			ResultSet rs = prepStatement.executeQuery();
			while(rs.next()){
				int user1 = rs.getInt("user1_id");
				int user2 = rs.getInt("user2_id");
				if(user1 == userId)
					recommendationsId.add(user2);
				else
					recommendationsId.add(user1);
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		//fetch user from database
		for(int id: recommendationsId){
			User user = new User(id);
			if(user.getId() == User.INVALID_USER)
				continue;
			recommendations.add(user);
		}
		
		

		return recommendations;
		
		
	}
	
}
