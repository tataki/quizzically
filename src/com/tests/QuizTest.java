package com.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.backend.DBObject;
import com.backend.QuizManager;
import com.models.Quiz;
import com.models.questions.*;

public class QuizTest {
	DBObject db = new DBObject();
	
	@Test
	public void questionTest() {
		
		fail("Not yet implemented");
	}

	@Test
	public void test() {
		QuizManager qm = new QuizManager();
		List<Quiz> result= qm.getSimilarQuizzes("Match");
		System.out.println(result);
		List<Quiz> matchResult1 = qm.getRelevantQuizzes("hard");
		System.out.println(matchResult1);
		List<Quiz> matchResult2 = qm.getRelevantQuizzes("Match");
		System.out.println(matchResult2);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testJSON() {
		ArrayList<BaseQuestion> questions1 = new ArrayList<BaseQuestion>();
		questions1.add(new FillInBlanksQuestion("question1", "answer1"));
		// questions1.add(new MultipleChoiceQuestion("question2", Arrays.asList("answer1", "answer2")));
		String json = "[{\"isTimed\":false,\"time\":0,\"question\":\"question1\",\"answer\":{\"answer\":\"answer1\"}},{\"isTimed\":false,\"time\":0,\"question\":\"question2\",\"answer\":{\"answers\":[\"answer1\",\"answer2\"]}}]";
		ArrayList<BaseQuestion> questions2 = (ArrayList<BaseQuestion>) QuestionGSON.jsonToQuestion(json);
		//assertEquals(questions1.get(0))
	}
	
}
