package com.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.backend.TagManager;
import com.models.Tag;

public class TagTest {
	@Before
	public void setup(){
		Tag tag1 = new Tag(1,5,"hard problem");
		tag1.upload();
		Tag tag2 = new Tag(1,5,"easy problem");	
		tag2.upload();
		Tag tag3 = new Tag(1,3,"funny");
		tag3.upload();
		Tag tag4 = new Tag(3,3,"boring");
		tag4.upload();
		Tag tag5 = new Tag(3,7,"challenging");
	
	}
	@Test
	public void uploadTest(){
		Tag tag = new Tag(1,3,"hard problem");
		assertEquals(true,tag.upload());
		tag.setTag("physics");
		tag.setQuiz_id(3);
		assertEquals(true,tag.upload());
	}
	
	@Test
	public void deleteTest(){
		Tag tag = new Tag(1,3,"hard problem");
		assertEquals(true,tag.delete());
		tag.setTag("physics");
		tag.setQuiz_id(3);
		assertEquals(true,tag.delete());
	}
	
	@Test
	public void fatchTagManagerTest(){
		TagManager tm = new TagManager();
		System.out.println(tm.fetchTagsForQuiz(1));
		System.out.println(tm.fetchTagsForQuiz(2));
		System.out.println(tm.fetchTagsForUser(2));
		System.out.println(tm.fetchTagsForUser(3));
	}
	
}
