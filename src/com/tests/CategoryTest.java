package com.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.models.Category;

public class CategoryTest {
	@Test
	public void testUpload(){
		Category cat = new Category("Test");
		assertEquals(true,cat.upload());
		assertEquals(true,cat.updateName("Test2"));
		assertEquals(true,cat.delete());
		Category.setCategoryTable();
	}
	
}
