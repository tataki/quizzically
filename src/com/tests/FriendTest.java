package com.tests;

import java.util.List;

import org.junit.Test;

import com.backend.FriendManager;
import com.backend.FriendRecommendation;
import com.models.Friend;
import com.models.User;

public class FriendTest {
	@Test
	public void uploadTest(){
		Friend friend = new Friend();
		friend.setValues(1, 13, 2);
		friend.upload();
		friend.setValues(1, 13, 1);
		friend.upload();
		friend.setValues(1, 13, 3);//update type
		friend.upload();
		friend.setValues(1, 5, 3);
		friend.upload();
		friend.setValues(1, 15, 3);
		friend.upload();
		friend.setValues(13, 3, 3);
		friend.upload();
		friend.setValues(3, 11, 3);
		friend.upload();
		friend.setValues(3, 7, 3);
		friend.upload();
		friend.setValues(3, 9, 3);
		friend.upload();
		friend.setValues(11,15, 3);
		friend.upload();
	}
	
	@Test
	public void getFriendsTest(){
		FriendManager fm = new FriendManager();
		System.out.println(fm.getFriends(3));		
	}
	
	@Test
	public void RecommendationTest(){
		FriendRecommendation fr = new FriendRecommendation();
		fr.setFriendRecommendationTable();
		
	}
	@Test
	public void DynamicRecommendationTest(){
		Friend friend = new Friend(1,3,3);
		friend.upload();
		friend.setValues(5, 11, 3);
		FriendManager fm = new FriendManager();
		List<User> rec = fm.getRecommendation(1);
		System.out.println();
		for(User user:rec)
			System.out.println(user.getId());
		
		rec = fm.getFriends(1, "user");
		System.out.println(rec);
		rec = fm.getRecommendation(1, "user");
		System.out.println(rec);
		
	}
}
