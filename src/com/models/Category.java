package com.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.backend.DBObject;
//import com.sun.crypto.provider.RSACipher;

public class Category extends DBObject {
	
	/*
	+-------+--------------+------+-----+---------+----------------+
	| Field | Type         | Null | Key | Default | Extra          |
	+-------+--------------+------+-----+---------+----------------+
	| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
	| name  | varchar(130) | NO   |     | NULL    |                |
	+-------+--------------+------+-----+---------+----------------+
	 */
	private int id;
	private String name;
	private int category_id = INVALID_CATEGORY;
	private static int INVALID_CATEGORY = -1;
	private static String insertCategory = "INSERT INTO "+ DBObject.categoryTable + " VALUE(null,?)";
	private static String updateCategory = "UPDATE "+ DBObject.categoryTable +" SET name = ? WHERE name = ?";
	private static String selectCategory = "SELECT * FROM " + DBObject.categoryTable + " WHERE name = ?";
	private static String deleteCategory = "DELETE FROM " + DBObject.categoryTable + " WHERE name = ?";
	
	private static String selectCategoryById = "SELECT * FROM " + DBObject.categoryTable + " WHERE id = ?";
	
	public static enum categoryType {
		general { public String toString() { return "General"; } },
		animals { public String toString() { return "Animals"; } },
		computers { public String toString() { return "Computers"; } },
		currentEvents { public String toString() { return "Current Events"; } },
		entertainment { public String toString() { return "Entertainment"; } },
		geography { public String toString() { return "Geography"; } },
		history { public String toString() { return "History"; } },
		people { public String toString() { return "People"; } },
		mathematics { public String toString() { return "Mathematics"; } },
		sports { public String toString() { return "Sports"; } },
		politics { public String toString() { return "Politics"; } },
		social { public String toString() { return "Social"; } },
		educational { public String toString() { return "Educational"; } }
	}
	/**
	 * Upload all the CategoryType to Quiz_catrgory table
	 * Usage:
	 * 
	 * 	if(!Category.setCatrgoryTable())
	 * 		System.out.println("These categories might already exist in DB");
	 * 		
	 * @return false the fields are fail to be uploaded.
	 */
	public static boolean setCategoryTable(){
		for(categoryType category : categoryType.values()){
			Category cat = new Category(category.toString());
			if(!cat.upload())
				return false;
		}	
		return true;
	}
	
	/**
	 * Constructor that takes an id and fetches the corresponding entry from the database.
	 * 
	 * @param id
	 */
	public Category(int id) {
		if(!conPrepare(selectCategoryById)) return;
		try {
			prepStatement.setInt(1, id);
			ResultSet rs = prepStatement.executeQuery();
			if (rs.next()) {
				this.id = id;
				this.name = rs.getString("name");
			}
		} catch (SQLException e) {}
	}
	
	public Category(String name) {
		this.name = name;
	}
	
	/**
	 * Create an category.
	 * Usage:
	 * 	Category cat = new Category(name);
	 * 	if(cat.upload())
	 * 		System.out.println(cat.getName()+"is a category with id " +cat.getId());
	 * else
	 * 		System.out.printIn("fail to create the category");
	 * @return true if the upload is successful.
	 */
	public boolean upload(){
		if(fetch() == false){	//the category not in Quiz_category yet	
			//case: normal category insert.
			System.out.println(insertCategory);
			if(!conPrepare(insertCategory))return false;
			try {
				prepStatement.setString(1, this.name);
				prepStatement.execute();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}			
			
		}

		return true;
	}
	/**
	 * Update Category Name. 
	 * Usage: 
	 * Category cat = new Category("Bla");
	 * if(!cat.updateName("Others"))
	 * 		system.out.println("fail to update category.");
	 * @param newName
	 * @return false if the update fails.
	 */
	public boolean updateName(String newName){
		
		if(!conPrepare(updateCategory)) return false;
		try {
			prepStatement.setString(1,newName);
			prepStatement.setString(2,name);
			if(prepStatement.executeUpdate()==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		name = newName;
		return true;
		
	}
	/**
	 * fetches category fields from Quiz_category, returns false if there there is not such category.
	 * Usage:
	 * Category cat = new Category("science");
	 * if(cat.fetch())
	 * 		int CategoryId = cat.getId();
	 * else
	 * 		error...
	 * 
	 * @param name
	 * @return
	 */
	public boolean fetch(){
		if(name == null) return false;//this is not going to be useful, just in case someone add Category();
		if(!conPrepare(selectCategory)) return false;
		try {
			prepStatement.setString(1, name);
			ResultSet rs = prepStatement.executeQuery();
			if(rs.next())
				category_id = rs.getInt("id");
			else
				return false;
		} catch (SQLException e) {
				e.printStackTrace();
				return false;
		}	
		return true;
	}
	/**
	 * delete a category.
	 * Usage:
	 * Category cat = new Category("bla");
	 * if(cat.delete())
	 * 		//delete the cstegory successfully.
	 * Note: Deleting a non existing category is considered legal.
	 * @return
	 */
	public boolean delete(){
		if(name == null) return false;//this is not going to be useful, just in case someone add Category();
		if(!conPrepare(deleteCategory)) return false;
		try {
			prepStatement.setString(1, name);
			prepStatement.execute();
		} catch (SQLException e) {
				e.printStackTrace();
				return false;
		}	
		return true;
	}
	//----------- getters-----------//
	public int getId(){
		return id;
	}
	
	public String getName(){
		return name;
	}
	//-------end of getters-------//
}
