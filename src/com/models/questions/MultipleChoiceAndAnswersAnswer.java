package com.models.questions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MultipleChoiceAndAnswersAnswer extends BaseAnswer {
	private List<String> choices;
	private List<String> mcaa_answers;
	public MultipleChoiceAndAnswersAnswer(List<String> answers, List<String> choices) {
		super(null);
		this.mcaa_answers = new ArrayList<String>(answers);
		this.choices = new ArrayList<String>(choices);
	}
	
	@Override
	public int checkAnswer(List<String> userAnswers) {
		Set<String> userAnswersSet = new HashSet<String>(userAnswers);
		Set<String> answersSet = new HashSet<String>(mcaa_answers);
		userAnswersSet.retainAll(answersSet);
		return userAnswersSet.size();
	}
	@Override
	public int points() {
		return (mcaa_answers != null ? mcaa_answers.size() : 0);
	}
	
	public List<String> getChoices() {
		return choices;
	}
	
}
