package com.models.questions;

import java.util.ArrayList;
import java.util.List;

public class MatchingAnswer extends BaseAnswer {
	private List<String> answers;
	
	public MatchingAnswer(List<String> answers){
		this.answers = new ArrayList<String>(answers);
	}
	

	/**
	 * Assumes that the userAnswers list passed in is 
	 * in right order that the user takes them in, returns
	 * whether these answers match up with our list of answers
	 * entry by entry
	 */
	@Override 
	public int checkAnswer(List<String> userAnswers) {
		int count = 0;
		if(answers.size() != userAnswers.size())
			return 0;
		for(int i = 0; i < userAnswers.size(); i++) {
			System.out.println("------- in MatchingAnswer.java ------------");
			System.out.println("answer should be: " + answers.get(i) + " >   given is : " + userAnswers.get(i));
			System.out.println("------- in MatchingAnswer.java -------------");
			if(answers.get(i).equals(userAnswers.get(i)))
				count++;
		}
		return count;
	}
	@Override
	public int points() {
		return answers.size();
	}
	
	public List<String> getAnswers() {
		return answers;
	}
}
