package com.models.questions;

import java.util.List;

public class MultipleChoiceAndAnswersQuestion extends BaseQuestion {
	
	MultipleChoiceAndAnswersAnswer mcaa;
	
	public MultipleChoiceAndAnswersQuestion(String question, List<String> answers, List<String> choices) {
		super(question, null);
		this.question = question;
		this.isTimed = false;
		this.mcaa = new MultipleChoiceAndAnswersAnswer(answers, choices);
		this.type = QuestionType.MultiChoiceMultiAnswer;
	}
	
	public List<String> getChoices() {
		return mcaa.getChoices();
	}
	
	@Override
	public int checkAnswer(List<String>userAnswers) {
		return mcaa.checkAnswer(userAnswers);
	}
	
	@Override
	public int points() {
		return mcaa.points();
	}
}
