package com.models.questions;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class QuestionGSON {
	
	private static Gson gs = new Gson();
	private static JsonParser parser = new JsonParser();
	
	public static String questionToJSON(List<BaseQuestion> questions) {
		return gs.toJson(questions);
	}
	
	/**
	 * Downcast the JsonElement passed in to a certain descendant of 
	 * the base object BaseQuestion.java. We upcast again when 
	 * returning so that we can always return a generic BaseQuestion type. 
	 * 
	 * @param obj - a JSON representation of a certain question type.
	 * Must inherit from BaseQuestion.java.
	 * @return BaseQuestion, by virtue of an upcast again.
	 */
	private static BaseQuestion downCastQuestionType(JsonElement obj) {
		BaseQuestion parsedQuestion= gs.fromJson(obj, BaseQuestion.class);
		
		if(parsedQuestion.getType() == null) {
			return gs.fromJson(obj, BaseQuestion.class);
		}
		if(parsedQuestion.getType().equals(QuestionType.Matching)) {
			return gs.fromJson(obj, MatchingQuestion.class);
		} 
		else if(parsedQuestion.getType().equals(QuestionType.MultiAnswer)) {
			return gs.fromJson(obj, MultipleAnswersQuestion.class);
		}
		else if(parsedQuestion.getType().equals(QuestionType.MultiChoice)) {
			return gs.fromJson(obj, MultipleChoiceQuestion.class);
		}
		else if(parsedQuestion.getType().equals(QuestionType.MultiChoiceMultiAnswer)) {
			return gs.fromJson(obj, MultipleChoiceAndAnswersQuestion.class);
		}
		else if(parsedQuestion.getType().equals(QuestionType.PictureResponse)) {
			return gs.fromJson(obj, PictureResponseQuestion.class);
		}
		// it's just a default type
		else {
			return gs.fromJson(obj, BaseQuestion.class);
		}
	}
	
	/*
	 * we cannot do json to Question passing in only ArrayList.class.
	 * We need to first transform the json into a lower level primitive
	 * and then use gson to parse each of the entries
	 */
	public static Object jsonToQuestion(String json) {
		JsonArray array = parser.parse(json).getAsJsonArray();
		List<BaseQuestion> results = new ArrayList<BaseQuestion>(array.size());
		
		for(int i = 0; i < array.size(); i++) {
			
			BaseQuestion toAdd = downCastQuestionType((JsonElement)array.get(i));
			
			// should always be reached
			if(toAdd != null) {
				results.add(toAdd);
			} else { /* should never reach here */ break; }
			
		}
		return results;
	}
	
}
