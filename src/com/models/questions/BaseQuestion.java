package com.models.questions;

import java.util.*;

public class BaseQuestion {
	
	protected boolean isTimed;
	protected int time;
	protected String question;
	protected BaseAnswer answer;
	protected QuestionType type;
	
	protected BaseQuestion() {}
	
	public BaseQuestion(String question, String answer) {
		this.question = question;
		this.isTimed = false;
		this.type = QuestionType.QuestionResponse;
		this.answer = new BaseAnswer(answer);
	}
	
	public QuestionType getType() {
		return type;
	}

	public String getQuestion() {
		return question;
	}
	
	/**
	 * Checks the answer and returns the number of correct
	 * answers. 
	 * Override if your answer is of a different type!
	 * @param userAnswers
	 * @return
	 */
	public int checkAnswer(List<String> userAnswers) {
		return answer.checkAnswer(userAnswers);
	}
	
	public void setTime(int time) {
		if (time == 0) {
			this.isTimed = false;
		}
		else {
			this.isTimed = true;
			this.time = time;
		}
	}
	
	public int getTime() {
		return this.time;
	}
	
	public boolean isTimed() {
		return this.isTimed;
	}
	
	
	/**
	 * Gets the number of points this question is worth.
	 * Override if your answer is of a different type!
	 * @return
	 */
	public int points() {
		return answer.points();
	}
	
	// debug
	
	public String printAnswer() {
		if(answer != null)
			return answer.getAnswer();
		else return null;
	}
}
