package com.models.questions;

import java.util.List;

public class MultipleChoiceQuestion extends BaseQuestion {
	
	private MultipleChoiceAnswer multipleChoiceAnswer;
	
	public MultipleChoiceQuestion(String question, String answer, List<String> choices) {
		super(question, answer);
		this.isTimed = false;
		this.multipleChoiceAnswer = new MultipleChoiceAnswer(answer, choices);
		// for multiple choice, first answer is the correct one;
		this.type = QuestionType.MultiChoice;
	}
	
	@Override
	public int checkAnswer(List<String> userAnswers) {
		return multipleChoiceAnswer.checkAnswer(userAnswers);
	}
	
	public List<String> getChoices() {
		return multipleChoiceAnswer.getChoices();
	}
	
}
