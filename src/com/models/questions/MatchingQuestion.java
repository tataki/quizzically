package com.models.questions;

import java.util.ArrayList;
import java.util.List;

public class MatchingQuestion extends BaseQuestion {
	
	private List<String> questions = new ArrayList<String>(); // note the -s ; not BaseQuestion's question field
	private MatchingAnswer matchingAnswer;
	
	public MatchingQuestion(List<String> questions, List<String> answers) {
		super(null, null);
		this.questions = questions;
		this.isTimed = false;
		this.matchingAnswer = new MatchingAnswer(answers);
		this.type = QuestionType.Matching;
	}
	
	public List<String> getQuestions() {
		return questions;
	}
	
	public List<String> getAnswers() {
		return this.matchingAnswer.getAnswers();
	}

	@Override
	public int points() {
		return (questions != null ? questions.size() : 0);
	}
	
	@Override
	public int checkAnswer(List<String> userAnswers) {
		return matchingAnswer.checkAnswer(userAnswers);
	}
}
