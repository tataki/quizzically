package com.models.questions;

import java.util.List;

public class MultipleAnswersQuestion extends BaseQuestion {
	
	private MultipleAnswersAnswer multipleAnswersAnswer;
	
	public MultipleAnswersQuestion(String question, List<String> answers, boolean inOrder) {
		this.question = question;
		this.isTimed = false;
		this.multipleAnswersAnswer = new MultipleAnswersAnswer(answers, inOrder);
		this.type = QuestionType.MultiAnswer;
	}

	public List<String> getAnswers() {
		return this.multipleAnswersAnswer.getAnswers();
	}
	
	@Override 
	public int checkAnswer(List<String> userAnswers) {
		return this.multipleAnswersAnswer.checkAnswer(userAnswers);
	}
	
	@Override
	public int points() {
		return this.multipleAnswersAnswer.getAnswers().size();
	}
}
