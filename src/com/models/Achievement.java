package com.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.backend.DBObject;
	
   /*
	+-------------+----------+------+-----+---------+----------------+
	| Field       | Type     | Null | Key | Default | Extra          |
	+-------------+----------+------+-----+---------+----------------+
	| id          | int(11)  | NO   | PRI | NULL    | auto_increment |
	| user_id     | int(11)  | NO   | MUL | NULL    |                |
	| award       | longtext | NO   |     | NULL    |                |
	| description | longtext | NO   |     | NULL    |                |
	| url         | longtext | NO   |     | NULL    |                |
	| timestamp   | datetime | NO   |     | NULL    |                |
	+-------------+----------+------+-----+---------+----------------+
	*/

/**
 * List of user's achievements.
 * To add one of the preformatted achievements:
 * 	
 * 		Achievement myNewA = Achievement.AMATEUR_AUTHOR(34); // loads new Ach. for user_id 34
 * 		myNewA.upload(); 
 * 		myNewA = null; // free this newly created object
 * 
 * @author jimzheng
 *
 */

public class Achievement extends DBObject {
	private int user_id;
	private String description;
	private String award;
	private String url;
	private Date timestamp;
	private int id;
	
	// urls that point o images of prizes for a certain level of award
	public static String DEFAULT_URL= "http://a.dryicons.com/images/icon_sets/luna_blue_icons/png/128x128/prize_winner.png";
	public static String LVL1_URL = "";
	public static String LVL2_URL = "";
	public static String LVL3_URL = "";
	public static String LVL4_URL = "";
	
	/* the required awards */
	public static Achievement AMATEUR_AUTHOR(int userid) {
		return new Achievement(userid, "You've created a quiz!", "Amateur Author", DEFAULT_URL, null);
	}
	public static Achievement PROLIFIC_AUTHOR(int userid) {
		return new Achievement(userid, "You've created 5 quizzes!", "Prolific Author", DEFAULT_URL, null);
	}
	public static Achievement PRODIGIOUS_AUTHOR(int userid) {
		return new Achievement(userid, "You've created 10 quizzes!", "Prodigious Author", DEFAULT_URL, null);
	}
	public static Achievement QUIZ_MACHINE(int userid) {
		return new Achievement(userid, "You've taken 10 quizzes!", "Quiz Machine", DEFAULT_URL, null);
	}
	public static Achievement I_AM_THE_GREATEST(int userid, String quiz_name) {
		return new Achievement(userid, "You've had a higest score on the quiz \"" + quiz_name + "\"!", "I am the Greatest", DEFAULT_URL, null);
	}
	public static Achievement PRACTICE_MAKES_PERFECT(int userid) {
		return new Achievement(userid, "You've taken a quiz in practice mode!", "Practice Makes Perfect", DEFAULT_URL, null);
	}

	public Achievement(int user_id, String description, String award, String url, Timestamp timestamp) {
		this.user_id = user_id;
		this.setDescription(description);
		this.setAward(award);
		this.setUrl(url);
		Date d = null;
		try {
			if (timestamp != null)
				d = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(timestamp.toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.setTimestamp(d);
	}
	
	private String insert = "INSERT into " + DBObject.achievementTable + " VALUE (NULL, ?, ?, ?, ?, NOW())";
	/**
	 * inserts into current database
	 * @return
	 */
	public boolean upload() {
		if(! this.conPrepare(insert)) return false;
		try {
			prepStatement.setInt(1, this.user_id);
			prepStatement.setString(2, this.award);
			prepStatement.setString(3, this.description);
			prepStatement.setString(4, this.url);
			prepStatement.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * deletes something only if it exists
	 */
	private String delete = "DELETE FROM " + DBObject.achievementTable + " WHERE id = ?";
	public void delete() {
		if(this.id < 1) return; 
		if(!this.conPrepare(delete)) return;
		try {
			prepStatement.setInt(1, this.id);
			prepStatement.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
			return;
		}
		return;
	}
	
	/**
	 * Check something if it exists.
	 */
	private String existsString = "SELECT * FROM " + DBObject.achievementTable + " WHERE user_id = ? AND description = ?";
	public boolean exists() {
		if(!this.conPrepare(existsString)) return false;
		try {
			prepStatement.setInt(1, user_id);
			prepStatement.setString(2, description);
			ResultSet rs = prepStatement.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setAward(String award) {
		this.award = award;
	}

	public String getAward() {
		return award;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setTimestamp(Date date) {
		this.timestamp = date;
	}

	public Date getTimestamp() {
		return timestamp;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
}
