package com.models;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.backend.DBObject;

public class Rating extends DBObject {
	/**
	 * when user rate a quiz, the front end calls update rating though a rating object.
	 * Rating class also update the rating summary in quiz table.
	 * 
	 */
	private int id;
	private int rating;
	private int user_id;
	private int quiz_id;
	private String insertQuery = "INSERT INTO " + DBObject.ratingTable + " VALUE(null,?,?,?)";
	private String updateQuery = "UPDATE " + DBObject.ratingTable + " SET rating = ? WHERE user_id = ? AND quiz_id = ?";		
	private String selectAllRatingQuery = " FROM " + DBObject.ratingTable + " WHERE quiz_id = ";
	private String selectRatingQuery = "SELECT * FROM "+ DBObject.ratingTable +" WHERE quiz_id = ";
	public Rating(int rating,int user_id,int quiz_id){
		super();
		this.rating = rating;
		this.user_id = user_id;
		this.quiz_id = quiz_id;
	}

	/**
	 * Usage: 
	 * Rating rating = new Rating(3,3,5);
	 * rating.upload();
	 * upload() will upload the current rating to the Quiz_rating and Quiz_quiz class.
	 * If the same user rated the same quiz two times, the old rating is updated by new rating.
	 * @return
	 */
	public boolean upload(){
		int newRating = rating;//record the old rating
		boolean isExist = fetch(quiz_id,user_id);
		
		//if it's an update keep the old data to update the rating in 
		try {
			if(isExist){
				if(newRating!=rating){
					if(!conPrepare(updateQuery))return false;				
					prepStatement.setInt(1, rating);
					prepStatement.setInt(2, user_id);
					prepStatement.setInt(3, quiz_id);		
					prepStatement.executeUpdate();
					Quiz q = new Quiz();
					Quiz.Rating newRatingRating = q.new Rating(newRating,quiz_id);
					Quiz.Rating ratingRating = q.new Rating(rating,quiz_id);
					return q.updateRating(ratingRating,newRatingRating);
				}
			}else{		
				//insert
				if(!conPrepare(insertQuery)) return false;
				prepStatement.setInt(1, rating);
				prepStatement.setInt(2, user_id);
				prepStatement.setInt(3, quiz_id);
				prepStatement.execute();
				Quiz q = new Quiz();
				Quiz.Rating quizRating = q.new Rating(rating,quiz_id);
				return q.addRating(quizRating);
			}			
		} catch (SQLException e) {			
			e.printStackTrace();
			return false;
		}
		
		return true;		
	}
	/**
	 * For getting current rating of a quiz from certain user.
	 * Usage: 
	 * Rating r = Rating();
	 * r.fetch(quiz_id,user_id);
	 * rating = r.getRating();
	 * 
	 * @param quiz_id
	 * @param user_id
	 * @return
	 */
	public boolean fetch(int quiz_id,int user_id){
		this.quiz_id = quiz_id;
		ResultSet rs = getResults(selectRatingQuery + quiz_id + " AND user_id = " + user_id);
		try {
			if(!rs.next()) return false;
			id = rs.getInt("id");
			rating = rs.getInt("rating");
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
		
		
	}
	public int getNumRated(int quiz_id){
		
		ResultSet rs = getResults("SELECT rating " + selectAllRatingQuery +  Integer.toString(quiz_id));
		int count = 0;
		
		try {
			while(rs.next()){
			count += rs.getInt("rating");
			}
		} catch (SQLException e) {
				e.printStackTrace();
				return 0;
		}
		return 0;
	}
	
	public int getRating(){
		return  rating;
	}
	
}
