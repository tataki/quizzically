package com.models;

// import java.sql.Time;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import com.backend.DBObject;

public class Message extends DBObject {
	
	/*
		+-------------+--------------+------+-----+---------+----------------+
		| Field       | Type         | Null | Key | Default | Extra          |
		+-------------+--------------+------+-----+---------+----------------+
		| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
		| fromUser_id | int(11)      | NO   | MUL | NULL    |                |
		| toUser_id   | int(11)      | NO   | MUL | NULL    |                |
		| quiz_id     | int(11)      | NO   | MUL | NULL    |                |
		| message     | longtext     | NO   |     | NULL    |                |
		| isRead      | tinyint(11)  | NO   |     | NULL    |                |
		| messageType | varchar(130) | NO   |     | NULL    |                |
		| timestamp   | datetime     | NO   |     | NULL    |                |
		+-------------+--------------+------+-----+---------+----------------+
	 */
	
	private int id;
	private int fromUserId;
	private int toUserId;
	private String message;
	private boolean isRead;
	private String messageType;
	private Date timestamp;
	private int quiz_id = Message.INVALID;
	public static int INVALID = -1;
	public static String []types = {"challenge", "note", "friend"};
	
	public String updateReadQuery = "UPDATE "  + DBObject.messageTable + " SET isRead = 1 where id = ?";
	public String selectQuery = "select * from " + DBObject.messageTable + " WHERE id = ?";
	
	/**
	 * Returns a specific type of message based on the parameters passed in.
	 * @param type
	 * @param fromUser_id
	 * @param toUser_id
	 * @param message
	 * @return
	 */
	public static Message getMessage(String type, int fromUser_id, int toUser_id, int quiz_id, String message) {
		Message toReturn = null;
		
		/* challenge  */
		if(type.equals(Message.types[0])) {
			toReturn = new Message(Message.INVALID, fromUser_id, toUser_id, quiz_id, "You have been given a challenge!",
					false, Message.types[0], null);
			toReturn.setMessageType(Message.types[0]);
		} 
		/* note */
		else if(type.equals(Message.types[1])) {
			toReturn = new Message(Message.INVALID, fromUser_id, toUser_id, quiz_id, message,
					false, Message.types[1], null);
			toReturn.setMessageType(Message.types[1]);
		} 
		/* friend */
		else if(type.equals(Message.types[2])) {
			toReturn = new Message(Message.INVALID, fromUser_id, toUser_id, quiz_id, null,
					false, Message.types[2], null);
			toReturn.setMessageType(Message.types[2]);
		} 
		
		return toReturn;
	}
	
	/* gets a message by its database id */
	public static boolean markAsRead(int id) {
		Message msg = new Message(id);
		return (msg.getId() != -1);
	}
	
	
	private Message (int id) {
		this.id = id;
		if(!conPrepare(updateReadQuery)) {
			this.id = -1;
			return; // fail
		}
		try {
			prepStatement.setInt(1, this.id);
			prepStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Message(int id, int fromUserId, int toUserId, int quiz_id, String message,
			boolean isRead, String messageType, Date timestamp) {
		super(DBObject.messageTable);
		this.id = id;
		this.fromUserId = fromUserId;
		this.toUserId = toUserId;
		this.message = message;
		this.isRead = isRead;
		this.quiz_id = quiz_id;
		this.messageType = messageType;
		this.timestamp = timestamp;
	}
	
	public boolean setRead(){
		if(conPrepare(updateReadQuery)) {
			/* continue */
		}
		try {
			prepStatement.setInt(1, this.id);
			prepStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		this.isRead = true;
		return true;
		
	}
	public int getId() {
		return id;
	}
	public int getFromUserId() {
		return fromUserId;
	}
	public int getToUserId() {
		return toUserId;
	}
	public String getMessage() {
		return message;
	}
	public boolean isRead() {
		return isRead;
	}
	public int getQuiz_id() {
		return quiz_id;
	}
	
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String type) {
		this.messageType = type;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	
	/**
	 * sync these fields with the database
	 */
	private String insertStr = "insert into " + DBObject.messageTable + 
						" VALUES " +
						" (NULL, ?, ?, ?, ?, 0, ?, NOW()); ";
						
	public boolean sync() {
		if(!this.conPrepare(insertStr)) return false;
		try {
			prepStatement.setInt(1, this.fromUserId);
			prepStatement.setInt(2, this.toUserId);
			prepStatement.setInt(3, this.quiz_id);
			if(this.message != null)
				prepStatement.setString(4, this.message);
			else
				prepStatement.setString(4, "");
			prepStatement.setString(5, this.messageType);
			return (prepStatement.executeUpdate() > 0);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
}
