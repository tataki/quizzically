package com.models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.backend.DBObject;

/**
 * A tag is something used in 
 * addition to categories to provide
 * another level of association.
 * These are user-generated. 
 * 
 * Tags are associated with both user and id,
 * and so keep track of both (a bit wasteful)
 * 
 * We also have a Quiz_tag_quiz Many-To-Many relationship
 * between quiz and tags. This means that whenever we insert
 * a tag or quiz, make sure to update this relations table so
 * that we can refer to it when necessary;
 * 
 * Database object:
 * 
	+---------+--------------+------+-----+---------+----------------+
	| Field   | Type         | Null | Key | Default | Extra          |
	+---------+--------------+------+-----+---------+----------------+
	| id      | int(11)      | NO   | PRI | NULL    | auto_increment |
	| user_id | int(11)      | NO   | MUL | NULL    |                |
	| tag     | varchar(130) | NO   |     | NULL    |                |
	+---------+--------------+------+-----+---------+----------------+
 
 * @author jimzheng
 *
 */

public class Tag extends DBObject{
	public final int INVALID_TAG_QUIZ_ID = -1;
	private int id; // could be user or quiz
	private int tag_quiz_id = -1;
	private int quiz_id;
	private int user_id;
	private int tag_id;

	private String tag;
	private String selectTagQuizBase = "SELECT * FROM " + DBObject.tagQuizRelationsTable;
	private String selectTagBase = "SELECT * FROM " + DBObject.tagTable;
	private String insertTagQuizQuery ="INSERT INTO " + DBObject.tagQuizRelationsTable + " VALUE (null,?,?)";
	private String insertTagQuery ="INSERT INTO "+ DBObject.tagTable + " VALUE (null,?,?)";
	private String selectTagQuery = selectTagBase+ " WHERE tag = ?";
	private String selectTagQuizQuery = selectTagQuizBase + " WHERE tag_id = ? AND quiz_id = ?";
	private String updateTagQuizQuery = "UPDATE " + DBObject.tagQuizRelationsTable + " SET tag_id = ?, quiz_id = ? WHERE id = ?";
	private String deleteTagQuizQuery = "DELETE FROM " + DBObject.tagQuizRelationsTable + " WHERE id = ?";
	private String deleteTagQuery = "DELETE FROM " + DBObject.tagTable + " WHERE id = ?";
	private String selectTagQuizByTagIdQuery = selectTagQuizBase + " WHERE tag_id = ?";
	
	public Tag(int id,int quiz_id, int user_id, String tag) {
		this.tag_quiz_id = id;
		this.quiz_id = quiz_id;
		this.user_id = user_id;
		this.tag = tag;
		
	}
	
	public Tag(int quiz_id, int user_id, String tag) {
		this.quiz_id = quiz_id;
		this.user_id = user_id;
		this.tag = tag;
	}
	
	
	/**
	 * upload can be used to upload or update a tag, quiz_id, user_id 
	 * usage:
	 * (upload)	
	 * Tag tag = new Tag(1,2,"basic")
	 * if(tag.upload())
	 * 		System.out.println("upload new tag successfully");
	 * 
	 * (update)
	 * Tag tag = new Tag(1,2,"basic")
	 * if(tag.upload())
	 * 		System.out.println("upload new tag successfully");
	 * tag.setTag("hard");
	 * tag.upload();
	 * 
	 * @return false if the upload fails.
	 */
	public boolean upload(){
		
		//insert tag if the tag doesn't exist.
		if(!fetchTag()){
		//insert
			Connection con = getConnection();		
			try {
					prepStatement = con.prepareStatement(insertTagQuery,statement.RETURN_GENERATED_KEYS);
					if(prepStatement==null) return false;
					prepStatement.setInt(1, user_id);
					prepStatement.setString(2, tag);
					prepStatement.execute();
					ResultSet rs = prepStatement.getGeneratedKeys();
					if(!rs.next()) return false;
					tag_id = rs.getInt(1);
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		boolean isExist = fetchTagQuiz();		
		if(tag_quiz_id == INVALID_TAG_QUIZ_ID &&!isExist){
			//insert TagQuiz
			Connection con = getConnection();		
			try {
					prepStatement = con.prepareStatement(insertTagQuizQuery,statement.RETURN_GENERATED_KEYS);
					if(prepStatement==null) return false;
					prepStatement.setInt(1, tag_id);
					prepStatement.setInt(2, quiz_id);
					prepStatement.execute();
					ResultSet rs = prepStatement.getGeneratedKeys();
					if(!rs.next()) return false;
					tag_quiz_id = rs.getInt(1);			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		if(tag_quiz_id!= INVALID_TAG_QUIZ_ID)
			return updateTagQuiz();
		
				
		return true;					
	}

	/**
	 * delete a tag attached to a quiz and a user. 
	 * Usage:
	 * Tag tag = new Tag(1,2,basic);
	 * if(tag.delete())
	 * 	 //delete the entry successfully.
	 * 
	 * or 
	 * 
	 * tag.delete();
	 * if(!tag.getId() == tag.INVALID_TAG_QUIZ_ID)
	 *	//delete the entry successfully
	 * @return false if fails to delete.
	 */
	
	public boolean delete(){
		//delete the tag_quiz table relation first
		if(tag_quiz_id == INVALID_TAG_QUIZ_ID){ 
			if(!fetch()) return true;
		}
		if(!conPrepare(deleteTagQuizQuery)) return false;
		try {
			prepStatement.setInt(1, tag_quiz_id);
			prepStatement.execute();
			tag_quiz_id = INVALID_TAG_QUIZ_ID;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	
		// it is consider a successful deletion even if tag is not deleted from tag table
		deleteTag();
		return true;									
	}
	
	public boolean fetch(){
		tag_quiz_id = INVALID_TAG_QUIZ_ID;
		return fetchTag() && fetchTagQuiz();
	}
	
	/**
	 * helper function to update Tag Quiz, it should be called only when tag_quiz_id is valid.
	 * @return 
	 */
	private boolean updateTagQuiz(){
		if(tag_quiz_id == INVALID_TAG_QUIZ_ID) return false;//called when tag_quiz_id is not set yet
		if(!conPrepare(updateTagQuizQuery)) return false;
		try {
			prepStatement.setInt(1, tag_id);		
			prepStatement.setInt(2, quiz_id);
			prepStatement.setInt(3, tag_quiz_id);
			if(prepStatement.executeUpdate()==0) return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;
	}
	/**
	 * Delete a tag that isn't referenced by any entry in tag_quiz table. 
	 * note: delete an non existing entry is consider legal.
	 * @return false if the operation does not success because of SQL error.
	 * 
	 */
	private boolean deleteTag(){
		//delete the tag in tag table if no entry in tag_quiz is referencing to the tag
		if(!conPrepare(selectTagQuizByTagIdQuery))return false;
		try {
			prepStatement.setInt(1,tag_id);
			ResultSet rs = prepStatement.executeQuery();
			if(rs.next()) return false;
			if(!conPrepare(deleteTagQuery))return false;
			prepStatement.setInt(1, tag_id);
			prepStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;
		
		
	}
	
	/**
	 * helper function for fetch.
	 */
	private boolean fetchTag(){
		if(!conPrepare(selectTagQuery))return false;
		try {
			prepStatement.setString(1, tag);
			ResultSet rs = prepStatement.executeQuery();
			if(!rs.next()) return false;
			tag_id = rs.getInt("id");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
	/**
	 * should only be called after TagQuiz. Helper function for fetch().
	 * @return
	 */
	private boolean fetchTagQuiz(){
		if(!conPrepare(selectTagQuizQuery))return false;		
			try {
				Connection con = getConnection();
				prepStatement.setInt(1, tag_id);
				prepStatement.setInt(2, quiz_id);
				ResultSet rs = prepStatement.executeQuery();
				if(!rs.next()) return false;
				tag_quiz_id = rs.getInt("id");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		return true;
	}
	
	
	//---------------getters & setters-----------------//
	public String getTag() {
		return tag;
	}


	/**
	 * @return the quiz_id
	 */
	public int getQuiz_id() {
		return quiz_id;
	}


	/**
	 * @param quiz_id the quiz_id to set
	 */
	public void setQuiz_id(int quiz_id) {
		this.quiz_id = quiz_id;
	}


	/**
	 * @param tag the tag to set
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}


	/**
	 * @return the tag_quiz_id
	 */
	public int getId() {
		return tag_quiz_id;
	}
	//------------end of getters and setters-------------//
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Tag [tag_quiz_id=" + tag_quiz_id + ", quiz_id=" + quiz_id
				+ ", user_id=" + user_id + ", tag=" + tag + "]";
	}

}
