package com.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map.Entry;

import org.eclipse.jdt.internal.compiler.ast.Statement;

import com.backend.DBObject;
import com.backend.FriendRecommendation;
import com.backend.FriendRecommendation.FriendPair;
	
	/*
	+------------+---------+------+-----+---------+----------------+
	| Field      | Type    | Null | Key | Default | Extra          |
	+------------+---------+------+-----+---------+----------------+
	| id         | int(11) | NO   | PRI | NULL    | auto_increment |
	| user1_id   | int(11) | NO   | MUL | NULL    |                |
	| user2_id   | int(11) | NO   | MUL | NULL    |                |
	| friendType | int(11) | NO   |     | NULL    |                |
	+------------+---------+------+-----+---------+----------------+
	*/

public class Friend extends DBObject {

	private int friendId1;
	private int friendId2;
	private int INVALIDTYPE = -1;
	private int INVALIDFRIENDSHIPID = -1;
	private int newType = INVALIDTYPE;
	private int type = INVALIDTYPE;
	private int friendShipId = INVALIDFRIENDSHIPID;	
	private String selectQuery= "SELECT * FROM " + DBObject.friendshipTable + " WHERE user1_id=? AND user2_id=?";
	private String updateFriendshipQuery = "UPDATE "+ DBObject.friendshipTable + " SET friendType=? WHERE " +
										"user1_id=? AND user2_id=?"; 
	private String insertFriendshipQuery = "INSERT INTO " + DBObject.friendshipTable + " VALUES(null,?,?,?)";
	private String deleteFriendshipQuery = "DELETE FROM " + DBObject.friendshipTable + " WHERE user1_id = ? OR user2_id = ?";
	public Friend(){
		super(DBObject.friendshipTable);
	}
	
	/**
	 * Constructor useful for checking whether f1 and f2
	 * are confirmed friends in the database.
	 * After calling this constructor, use fetch()
	 * and check if the type is 3.
	 * @param f1
	 * @param f2
	 */
	public Friend(int f1, int f2) {
		this.friendId1 = f1;
		this.friendId2 = f2;
	}
	
	public Friend(int friendShipId, int friendId1,int friendId2, int type){
		this.friendShipId = friendShipId;
		this.friendId1=friendId1;
		this.friendId2=friendId2;
		this.type = type;
	}
	/**
	 * not tested.
	 * @return true if userId1 and userId2 is friend.
	 */
	public boolean isFriend(){
		if(friendId1>friendId2){
			int temp = friendId1;
			friendId1 = friendId2;
			friendId2 = temp;
			if(type==1)
				type = 2;
			else
				type = 1;
		}
		if(this.friendShipId != this.INVALIDFRIENDSHIPID && this.type != this.INVALIDTYPE)
			if(fetch())
				if(this.type==3)
					return true;
		return false;
	}
	/**
	 * Delete is only called when a user is deleted from the database.
	 * This is a method to clean up the user info in friendship.
	 * @param user_id
	 * @return false if fails.
	 */
	public boolean delete(int user_id){
		if(!conPrepare(deleteFriendshipQuery)) return false;
		try {
			prepStatement.setInt(1, user_id);
			prepStatement.setInt(2, user_id);
			prepStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		FriendRecommendation fr = new FriendRecommendation();
		return fr.delete(user_id);
	}
	
	public Friend(int friendId1,int friendId2, int type){
		this.friendId1=friendId1;
		this.friendId2=friendId2;
		this.type = type;
	}
	/**
	 * inserts a relationship of friendId1 and friendId2 in to Quiz_FriendTable
	 * @param friendId1
	 * @param friendId2
	 * @param type
	 * @return
	 */
	public boolean upload(){
		//if the data exist do an update
		//Otherwise do an insert.
		FriendRecommendation fr = new FriendRecommendation();
		if(!fetch()){//does not exsit
			try {
				Connection con = getConnection();
				prepStatement = con.prepareStatement(insertFriendshipQuery, statement.RETURN_GENERATED_KEYS);				
				prepStatement.setInt(1,friendId1);
				prepStatement.setInt(2,friendId2);
				prepStatement.setInt(3,type);			
				prepStatement.execute();
				ResultSet rs =  prepStatement.getGeneratedKeys();
				if(!rs.next()) return false;
				this.friendShipId = rs.getInt(1);
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}		
		}else{
			try {			
				if(! this.conPrepare(updateFriendshipQuery)) return false;
				if(type == newType && newType == 3) return true;
				type = getResultType();				
				prepStatement.setInt(1,type);
				prepStatement.setInt(2,friendId1);
				prepStatement.setInt(3,friendId2);			
				prepStatement.executeUpdate();		
				
			} catch(SQLException e) {
				e.printStackTrace();
				return false;
			}
		}
		if(type == 3)
			fr.deleteRecommendation(friendId1, friendId2);
		return true;
	}
	/**
	 * Fetch the status about user1 and user2 from database. 
	 * Useful for checking the friendship status user1 and user2 
	 * @return
	 */
	public boolean fetch(){		
		if(friendId1>friendId2){
			int temp = friendId1;
			friendId1 = friendId2;
			friendId2 = temp;
			if(type==1)
				type = 2;
			else
				type = 1;
		}
		if(!conPrepare(selectQuery))return false;
		try {
			prepStatement.setInt(1, friendId1);
			prepStatement.setInt(2, friendId2);
			ResultSet rs = prepStatement.executeQuery();
			if(!rs.next()) return false;
			this.friendShipId = rs.getInt("id");
			this.newType = this.type;
			this.type = rs.getInt("friendType");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	//--------------------getters & setters--------------------//
	private int getResultType(){
		if(newType==INVALIDTYPE)
			return type;
		
		if(newType == 1 && type == 2)
			return 3;
		if(newType == 2 && type == 1)
			return 3;
		return newType;
	}
	/**
	 * @return the friendShipId
	 */
	public int getFriendShipId() {
		return friendShipId;
	}

	/**
	 * @param friendShipId the friendShipId to set
	 */
	public void setFriendShipId(int friendShipId) {
		this.friendShipId = friendShipId;
	}

	/**
	 * @return the friendId1
	 */
	public int getFriendId1() {
		return friendId1;
	}

	/**
	 * @param friendId1 the friendId1 to set
	 */
	public void setFriendId1(int friendId1) {
		this.friendId1 = friendId1;
	}

	/**
	 * @return the friendId2
	 */
	public int getFriendId2() {
		return friendId2;
	}

	/**
	 * @param friendId2 the friendId2 to set
	 */
	public void setFriendId2(int friendId2) {
		this.friendId2 = friendId2;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	
	public void setValues(int friendId1,int friendId2, int type){
		this.friendId1 = friendId1;
		this.friendId2 = friendId2;
		this.type = type;
	}
	//------------------end of getters and setters-------------------//
	
}
