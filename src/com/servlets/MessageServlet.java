package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.backend.MessageManager;
import com.google.gson.Gson;
import com.models.Friend;
import com.models.Message;
import com.models.User;

/**
 * Sydney
 * Servlet implementation class MessageServlet 
 * Receives POST data from the front end and does good work with it.
 * Does not retrieve Messages from the db; that is handled by MessageManager.
 * @@@not done yet
 */
@WebServlet("/MessageServlet")
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MessageServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("404.html");
	}

	/**
	 * Takes an action; reads in the session's ObjectManagers, and:
	 * 	1. If msg is a friend request, sends the friend request.
	 *  2. If message is a note or challenge, check that a friendship exists
	 *  		between two users.
	 * @param msg
	 */
	protected boolean interpretAction(HttpServletRequest request, Message msg) {
		int user1 = Math.min(msg.getFromUserId(), msg.getToUserId());
		int user2 = Math.max(msg.getFromUserId(), msg.getToUserId());
		int type = (msg.getFromUserId() == user1 ? 1 : 2);
		Friend friendship = new Friend(user1, user2, type);
		System.out.println("user1 is " + user1 + "user2 is " + user2 + ":type is " + type);
		if(msg.getMessageType().equals(Message.types[2])) {
			return friendship.upload();
		}
		/*
		 * if it's challenge, only allow challenges if you're already
		 * friends with the user
		 */
		else if(msg.getMessageType().equals(Message.types[0])) 
		{
			Friend check = new Friend(msg.getFromUserId(), msg.getToUserId());
			if(check.fetch() && check.getType() == 3) {
				return true;
			} else {
				return false;
			}
		}
		/* 
		 * if it's a note, we've already taken care of it, so do nothing
		 */
		else {
			System.out.println("note here");
		}
		return true;
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * MessageServlet takes in an ajax request and tries to interpret it as a message,
	 * nothing more.  
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		
		// some initial error-checking
		if(user == null) {
			response.getWriter().write("fail");
			return;
		}
		
		String message;
		User toUser;
		int fromUser_id, toUser_id, quiz_id, message_id;
		
		// if we're composing (i.e. sending a new friend request, creating a new note or challenge),
		// have the "compose" flag set
		if(request.getParameter("compose") != null) {
			/*
			 * from-user is the current session's user
			 * look up the toUser_id by doing lookup on the string passed in  
			 * quiz_id is target quiz_id in case it is a challenge
			 */
			message = request.getParameter("message");
			fromUser_id = user.getId();
			
			toUser = null;
			toUser_id = -1;
			
			/* try our very best to get the toUser field by
			 * looking at first the toUser_id, and then
			 * a username
			 */
			if(request.getParameter("toUser_id") != null) {
				try {
					toUser = new User(Integer.parseInt(request.getParameter("toUser_id"))); 
					toUser_id = toUser.getId();
				} catch (Exception e) {
					/* keep going */
				}
			}
			// if we haven't passed in a toUser_Id
			if(toUser == null) {
				toUser = new User(request.getParameter("username"));
				if(toUser != null) 
					toUser_id = toUser.getId();
			}
			// CHALLENGE - extra parameters
			// quiz_id - get if this is not null
			quiz_id = Message.INVALID;
			if(request.getParameter("quiz_id") != null) {
				quiz_id = Integer.parseInt(request.getParameter("quiz_id"));
			}
	
			// type of message indicates which one to use
			String type = request.getParameter("type");
		
			/* create the right type of message,
			 * interpret as an action and do the action.
			 * return a success string if we have both
			 * successfully synced and interpreted the 
			 * message
			 */
			Message msg = Message.getMessage(type, fromUser_id, toUser_id, quiz_id, message);
			String resultStr = "fail";
			
			if(interpretAction(request, msg)) {
				if(msg.sync())  
					resultStr = "success";
				else 
					System.out.println("syncing failed");
			}
			
			System.out.println("-------- > " + resultStr);
			/*
			 * We have composed and need to be sent a redirect
			 * back to main.
			 */
			if(request.getParameter("shouldRedirect") != null) {
				response.sendRedirect("main.jsp");
				return;
			}
			
			/* in our new scheme, this Servlet only responds to POST ajax requests,
			 * so this response should not return anything.
			 */
			PrintWriter pw = response.getWriter();
			pw.write(resultStr);
		}
		
		// not creating, just updating read status 
		else {
			message_id = -1;
			if(request.getParameter("message_id") != null) {
				message_id = Integer.parseInt(request.getParameter("message_id")); 
				Message.markAsRead(message_id);
				response.getWriter().write("success");
			}
		}
		
	}
}
