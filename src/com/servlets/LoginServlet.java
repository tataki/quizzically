package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.models.User;

//Author: Samir Patel

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		
		if(user != null && user.isAuthenticated()) {
			session.setAttribute("special", "29dd2f9f8d9312235caab2629e28ad45");
			response.sendRedirect("main.jsp");
			return;
		}
		
		String usr = (String) request.getParameter("username");
		String pwd = (String) request.getParameter("password");

		session.setAttribute("user", user);

		user = new User(usr);
		if(user.getId() != User.INVALID_USER) {
			user.authenticateUser(pwd);

			if(user.isAuthenticated() == false) {
				session.setAttribute("login_error_txt", "Login invalid!! Please login...");
				session.setAttribute("user", null);
				response.sendRedirect("main.jsp");				
				return;
			}
			
			Cookie userCookie = new Cookie("logged_in", "logged_in");
			userCookie.setValue(usr);
			userCookie.setMaxAge(3600);
			response.addCookie(userCookie);

			session.setAttribute("user", user);
			session.setAttribute("login_error_txt", "");
			session.setAttribute("special", "29dd2f9f8d9312235caab2629e28ad45");
			response.sendRedirect("main.jsp");
			return;
		}

		response.sendRedirect("login.jsp");
	}

}
