package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.models.User;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/LogoutServlet")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogoutServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");

		Cookie userCookie = new Cookie("logged_in", "logged_in");
		userCookie.setValue(" ");
		userCookie.setMaxAge(3600);
		response.addCookie(userCookie);

//		if(user == null) {
//			response.sendRedirect("login.jsp");
//			return;
//		}

//		user = null;
		session.setAttribute("user", null);
		response.sendRedirect("login.jsp");
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");

		Cookie cookie = new Cookie("logged_in", "logged_in");
		cookie.setValue("");
		cookie.setMaxAge(0);
		response.addCookie(cookie);

		if(user == null) {
			response.sendRedirect("index.html");
			return;
		}
		user = null;
		session.setAttribute("user", user);
		response.sendRedirect("login.jsp");
		return;
	}

}
