package com.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.models.Quiz;
import com.models.questions.BaseQuestion;

/**
 * Servlet implementation class QuizServlet
 */
@WebServlet("/QuizServlet")
public class QuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuizServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /* not really pure "random" shuffling, but does try to jumble up 
     * elements in the array
     */
    List<BaseQuestion> shuffle(List<BaseQuestion> bqlist) 
    {
    	List<BaseQuestion> shuffled = new ArrayList<BaseQuestion>(bqlist.size());
    	Random generator = new Random();
		while(! bqlist.isEmpty()) {
			int rand = generator.nextInt(bqlist.size());
			shuffled.add(bqlist.remove(rand));
		}
    	return shuffled;
    }
   
    /*
     * sends a redirect to a 404 page. Remember to call "return" after this function, as 
     * sendRedirect does not stop execution of your code.
     */
    protected void redirectTo404(HttpServletResponse response) throws ServletException, IOException {
    	response.sendRedirect("404.html");
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * 
	 * If GET request is valid, then it will contain a parameter "quizId" 
	 * that will initialize the quiz-taking process
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{

		HttpSession session = request.getSession();
		if(request.getParameter("quiz_id") == null) {
			redirectTo404(response);
			return;
		}
		int quizId = Integer.parseInt(request.getParameter("quiz_id"));
		
		// check safety and redirect to 
		// 404 if something seems fishy
		if(session == null || quizId < 0) {
			redirectTo404(response);
			return;
		}
		
		Quiz quiz = Quiz.fetch(quizId);
		List<BaseQuestion> questions = null;
		try {
			questions = (List<BaseQuestion>) Quiz.fetch(quizId).getQuestions();
			// if 'random' option is set for quiz, 
			// shuffle the array around
			questions = shuffle(questions);
			
			//Store the current quiz into session
			session.setAttribute("quiz", quiz);
			
			Integer curScore = new Integer(0);
			
			//Store default info to session
			session.setAttribute("questions", questions);
			session.setAttribute("curScore", curScore);
			if(quiz.getImmediate_feedback()) {
				session.setAttribute("immediate_feedback", true);
			}
			
			// if single page, set the curQuestion attribute
			// of our session variable; otherwise, set it to 
			// null to prevent accidental reading of old values
			if (!quiz.isSingle_page()) {
				Integer curQuestion = new Integer(0);
				session.setAttribute("curQuestion", curQuestion);
			} else {
				session.removeAttribute("curQuestion");
			}
			
			session.setAttribute("taking", true);
			session.setAttribute("startTime", System.currentTimeMillis());
			
			// pass things off to quiz.jsp to start the relay
			RequestDispatcher dispatch = request.getRequestDispatcher("quiz.jsp");
			dispatch.forward(request, response);
		} catch (Exception e) {
			redirectTo404(response);
			return;
		}
	}
	
	private String determineDispatchUrl(HttpSession session) {
		if(session.getAttribute("immediate_feedback") != null) {
			return "feedback.jsp";
		} else {
			return "quiz.jsp";
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * 
	 * Quiz-taking process hops back and forth between QuizServlet and Quiz.jsp.
	 * They communicate the following variable values via the session variable:
	 * 
	  		+ --------------------------------------------------------------------------- +
	  		| session attribute :  meaning 
	  		| -----------------------------------------------------------------------------
	  		| "questions" : List<BaseQuestions> we're using 
	        | "curQuestion" : current question index;
	  		|				exists if we're not in single page mode.
	  		| "curScore" : cumulative current score
	  		| "immediate_feedback" : boolean true if quiz should give immediate feedback
	  		------------------------------------------------------------------------------+
	 *
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		
		Boolean taking= (Boolean) session.getAttribute("taking");
		if(taking == null || taking == false) {
			response.sendRedirect("404.html");
			return;
		}
		
		// get session attributes so far
		Integer curQuestion = (Integer) session.getAttribute("curQuestion");
		Integer curScore = (Integer) session.getAttribute("curScore");
		List<BaseQuestion> questions = (List<BaseQuestion>) session.getAttribute("questions");
		
		//  if these don't exist, something has gone wrong
		if (curScore == null || questions == null) {
			response.sendRedirect("404.html");
			return;
		}
		
		/* meaningful work needs to be done. This means seeing whether the quiz is a single page
		 * or multi-page quiz, and then either advancing to the next question or going back to the
		 * We also have to handle special cases where the timer went off before the user 
		 * has successfully answered his or her questions. In this case, we skip evaluation
		 * of that question, give the user 0 points for that question, and move on.
		 * 
		 * Lastly, when we'r e redirecting, we have to check whether the creator has specified immediate
		 * feedback and if so, provide it for the user.
		 */
		
		RequestDispatcher dispatch;
		int score = 0;
		
		// Multi-page quiz
		// detect ending if we run off the end of the quiz
		if (curQuestion != null) {
			List<String> userAnswersList = null;;
			try {
				userAnswersList = Arrays.asList(request.getParameterValues("answer"));
				score = questions.get(curQuestion).checkAnswer(userAnswersList);
				session.setAttribute("curQuestion", new Integer(curQuestion.intValue() + 1));
				session.setAttribute("curScore", new Integer(curScore.intValue() + score));
			}
			catch (NullPointerException e) {
				/* means that our last question somehow had null answers in it, so continue */
			}
			
			// did we end ?
			if(curQuestion.intValue() >= questions.size() - 1) {
				session.setAttribute("endTime", System.currentTimeMillis());
				session.setAttribute("taking", false);
				dispatch = request.getRequestDispatcher("quiz-results.jsp");
			} 
			
			else {
				session.setAttribute("userAnswers", userAnswersList);
				dispatch = request.getRequestDispatcher(determineDispatchUrl(session));
				
				
			}
		}
		
		// Single-page quiz;
		// redirect to results page by checking all answers
		// answers come back from the client as one big POST string with params of the format:
		//       answer-1, answer-2, answer-2, answer-3, ... etc.. 
		//       where the number specifies which question
		else {
			System.out.println("------------------------------- ");
			for (int i = 0; i < questions.size(); i++) {
				try {
					List<String> userAnswersList = Arrays.asList(request.getParameterValues("answer-" + (i + 1)));
					
					System.out.println("user answers : " + userAnswersList.toString());
					System.out.println("answer: " + questions.get(i).printAnswer());
					
					score += questions.get(i).checkAnswer(userAnswersList);
					
				} 
				catch (NullPointerException e) {
					continue;
				}
			}
			System.out.println("------------------------------- ");
				
			session.setAttribute("curScore", score);
			session.setAttribute("endTime", System.currentTimeMillis());
			session.setAttribute("taking", false);
			dispatch = request.getRequestDispatcher("quiz-results.jsp");
		}
		dispatch.forward(request, response);
	}
}
