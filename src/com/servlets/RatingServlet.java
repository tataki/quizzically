package com.servlets;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Quiz;
import com.models.Rating;
import com.models.User;

/**
 * Servlet implementation class RatingServlet
 */
@WebServlet("/RatingServlet")
public class RatingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RatingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int rating = Integer.getInteger(request.getParameter("rating"));
		Quiz quiz = (Quiz) request.getSession().getAttribute("Quiz");
		User user = (User) request.getSession().getAttribute("User");
		if(quiz == null || user == null ){
			response.sendRedirect("404.html");
			return;
		}
		Rating ratingObject = new Rating(rating, quiz.getId(), user.getId());
		ratingObject.upload();
		//need to fill the path 
		RequestDispatcher dispatch = request.getRequestDispatcher("");
		dispatch.forward(request, response);
	}

}
