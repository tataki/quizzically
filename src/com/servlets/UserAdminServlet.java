package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.models.Announcement;
import com.models.Quiz;
import com.models.User;

/**
 * Servlet implementation class UserAdminServlet
 */
@WebServlet("/UserAdminServlet")
public class UserAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAdminServlet() {
        super();
        /* do nothing for now */
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* do nothing for now */
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * This is a front-facing Servlet that only takes in some parameters, tries to interpret them as Admin commands,
	 * and updates the database depending on what the "type" field is.
	 * Writes back to the response the string "success" on successful insertion of the command, and something
	 * else on failure.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pout = response.getWriter();

		String type = (String) request.getParameter("type");
		response.setContentType("text/html");

		if(type.equals("create-announcement")) {		
			ServletContext context = request.getServletContext();
			User user = (User) context.getAttribute("user");
			String announcement = (String) request.getParameter("announcement");
			int importance = Integer.parseInt(request.getParameter("importance"));
			Announcement.createAnnouncement(user.getId(), announcement, importance);
			response.sendRedirect("admin.jsp");
			return;
		}
		
		if(type.equals("delete-quiz")) {
			Quiz quiz = Quiz.fetch(Integer.parseInt(request.getParameter("quiz_id")));
			quiz.delete();
		}
		
		if(request.getParameter("user_id") != null) {			
			int userId = Integer.parseInt((String) request.getParameter("user_id"));
			
			User user = new User(userId);
			if(type.equals("make-admin")) {
				user.setAdmin(true);
			}
			if(type.equals("remove-admin")) {
				user.setAdmin(false);
			}
	
			if(type.equals("delete-user")) {
				user.deleteUser();
			}
			user.sync();
		}
		
		if(request.getParameter("announcement_id") != null) {
			int announcementId = Integer.parseInt((String) request.getParameter("announcement_id"));
			
			if(type.equals("delete-announcement")) {
				Announcement.deleteAnnouncement(announcementId);
			}			
		}
		
		pout.write("success");
		pout.close();
	}

}
