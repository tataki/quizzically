user: jimzheng (Zheng, Jim)
user: wawa3070 (Lin, Sydney)
user: yangyh (Yang, Yuan-Hung)
user: samir42 (Patel, Samir)

CS 108 Project Documentation

------------------------
|		Team Mach One      |
------------------------

-------------
| Features: |
-------------
# Multiple Question Types:
	We implemented all the basic question types, plus
	most of the extensions (excluding the "exotic" types):
		For matching, we implemented our own drag-n-drop system in javascript
			and allowed users to match elements using this system.
		For multiple answers and questions, we keep a list of saved questions
			and their various answers.
		For auto-generated questions, we keep track of when a question has been
			graded and notify the user when they next log in.

# Saved Quiz Progress:
	If the user stops taking a quiz and exits, we will save the progress
	data into a separate progress table. When the user next logs back
	in we can do an IF sql-statement to check whether the progress
	data exists, and if not, then we start a new quiz.

# Real-time Updates:
	If the user receives a new message or get an important
	announcement they will receive notification of it no matter
	when they're logging onto the site. We achieve this by having
	a function poll our servlets and see if there are new changes. 
	If there are, we use JQuery to update the application and
	notify the user.

# Recommendations List:
	Friends - We create a separate servlet to run in the backend every so often,
	getting recently updated rows of our friend system and making
	recommendations for friends.
	Quiz - We also implement recommendations at the quiz level, so that users
	can get a list of quizzes they may like based on their past experience
	with taking quizzes and their friends' taken quizzes.

---------------------
| Essential Features |
---------------------
	Question Types:
		Implemented question-response, fill-in-the-blank, multiple choice, 
				(EXTENSIONS):
				picture-response, multi-answer questions, multiple choice/multiple answer,
				matching, auto-generated, timed and graded.

	Quiz Options:
		Implemented one/multiple pages, randomized order, immediate corrections,
		practice quizzes.

	Scoring:
		For each question, we keep track of the # of possible points a user can earn.
		We aggregate these and find the total, then add up the total points the user
		received and add them to the database.
	
		When we finish recording everything, we place the information about the quiz
		into an "Activity" object that tracks the user's scores, when the user finished
		the quiz, and how long it took.
		
		When we need to query for top scores, we do a select call to the database and
		pull the top scores associated with a quiz_id. 
	
	Friends:
		We maintain a two-way friendship model; when a friend is first created, we
		maintain a list of blocks that are 

	Messages:
		We support the three types of messages : challenge, note, and friend request.

	Administration:
		We keep an admin.jsp view that serves all functionalities required by the
		assignment. 

	Overall Web Look:
		We designed our code with CSS3 and took pains to make the UI seem responsive,
		utilizing jQuery and web conventions to do so.
		We borrowed various components from online tutorials to make our GUI
		more interactive, incorporating tabs, modal popups, notifications, labels,
		textfields, tablesorter, buttons, etc... 
	
	Error-checking and Form Creation:
		We do detailed error-checking for form creation; the user is not permitted
		to enter a null quiz name.
		
	Sessions:
		All data is stored in a session variable that's visible to all servlets.
		Thus, we support multiple users by default.

----------------------------------------------
| Plugins Used (as permitted by instructor): |
----------------------------------------------
jQuery foundations - provides easy "tabbing" functionality on admin/main pages and other UI niceties
jQuery gritter - notification popup like Mac OSX growl.
Gson - Google open source library for parsing JSON to and from Java objects.

Note: jQuery tablesorter.js is included in our js package but never used.

	

