<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*, com.backend.*, com.models.*, java.text.DecimalFormat" %>
<%!
	double roundTwoDecimals(double d) {
	    DecimalFormat twoDForm = new DecimalFormat("#.#");
		return Double.valueOf(twoDForm.format(d));
	}
%>
<%
	/* Get results from our session
	* Record a new activity when we get to this page.
	*/
	
	// #1 Suspicious activity 
	// we are trying to get here from never having started
	// taking a quiz... suspicious activity...
	Boolean taking = (Boolean) session.getAttribute("taking");
	if(taking == null) {
		response.sendRedirect("404.html");
		return;
	}
	// #2 Weird redirection 
	// weird ; we hopped to results but we're not done yet
	if(taking == true) {
		response.sendRedirect("main.jsp");
		return;
	}
	
	// #3: Success
	// this is what we want; the user has just finished taking the quiz
	if(taking == false) {
		session.removeAttribute("taking");
	} else {
		/* should never reach here */
		System.exit(1);
	}
	
	/*
	* Read parameters from the session after presumably 
	* the user has finished taking the quiz
	*/
	ActivityManager activityManager = (ActivityManager) request.getServletContext().getAttribute("activityManager");
	Quiz quiz = (Quiz) session.getAttribute("quiz");
	User user = (User) session.getAttribute("user");
	if(quiz == null || user == null) {
		response.sendRedirect("404.html");
		return;
	}
	int totalScore = quiz.getPoints();
	int curScore = ((Integer) session.getAttribute("curScore")).intValue();
	double finalScore = (double)curScore / (double)totalScore;
	double finalPercentage = roundTwoDecimals(finalScore * (double)100);
	
	double startTime = ((Long) session.getAttribute("startTime")).doubleValue();
	double endTime = ((Long) session.getAttribute("endTime")).doubleValue();
	double highestScore = activityManager.getTopScoreByQuizId(quiz.getId());
	
	Achievement a = null, b = null;
	if (request.getParameter("practice") == null)
	{
		Activity result = Activity.TOOK_QUIZ(user.getId(), quiz.getId(), finalScore, (endTime - startTime));
		result.upload();
		
		if (activityManager.getQuizCountForUser(user.getId()) == 10) {
			a = Achievement.QUIZ_MACHINE(user.getId());
			if (!a.exists())
				a.upload();
			else
				a = null;
		}
		
		if (finalScore > highestScore) {
			b = Achievement.I_AM_THE_GREATEST(user.getId(), quiz.getName());
			if (!b.exists())
				b.upload();
			else
				b = null;
		}
	}
	else {
		a = Achievement.PRACTICE_MAKES_PERFECT(user.getId());
		if (!a.exists())
			a.upload();
		else
			a = null;
	}
	
%>    
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Welcome to Foundation</title>
  
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="stylesheets/foundation.css">
	<link rel="stylesheet" href="stylesheets/app.css">
	<link rel="stylesheet" href="stylesheets/reveal.css">
	<script src="javascripts/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
	<script src="javascripts/jquery.reveal.js" type="text/javascript"></script>

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="stylesheets/ie.css">
	<![endif]-->


	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>

	<ul class="topbar" style="width:100%; height:25px;">
		<li><a href="main.jsp">Home</a></li>
		<li><a href="search.jsp">Search</a></li>
		<li><a href="LogoutServlet">Logout</a></li>
	</ul>

	<!-- container -->
	<div class="container">
		
		<!-- Heading -->
		<div class="row" style="padding-top:30px">
			<div class="twelve columns">
				<div class="six columns">
					<div><img src="images/quizzically-logo-small.png" /></div>
					<p>Powered by Team Mach-One, cs108, Stanford University</p>
				</div>
				
			</div>
		<hr />
		</div>
		<!-- End Heading -->
		
		<%
			if (a != null && b != null) {
		%>
				<div style="display:none;" class="achievement">
					<h6>You've earned two achievements!</h6>
					<p><%=a.getAward()%> and <%=b.getAward()%></p>
				</div>
		<%
			}
			else if (a != null) {
		%>
				<div style="display:none;" class="achievement">
					<h6>You've earned an achievement!</h6>
					<p><%=a.getAward()%></p>
				</div>
		<%
			}
		%>
		
		<div class="row">
			<div class="twelve columns">
				<h3>Results </h3>
				<br/>
				<h5><img src="images/check.png" width="25px" height="25px" style="padding-right:10px;" />Number Correct: <%= curScore %></h5>
				<br/>
				<h5>Final Score: <%= curScore %>/<%= totalScore %> (<%= finalPercentage %> %) </h5>
				<hr/>
				<br/>
				<h5>Performance Comparison:</h5><br/>
				
				<!-- RESULTS TABS -->
				<dl class="contained tabs">
				  <dd><a href="#past" class="active">Your Past</a></dd>
				  <dd><a href="#top">Best Ever</a></dd>
				</dl>
				
				<!-- RESULTS TAB CONTAINER -->
				<ul class="nice tabs-content contained">
			
					<!-- YOUR OWN PAST RESULTS -->
					<li class="active" id="pastTab">
				  		<br/>
						<table width="800" id="user-performance-table" class="tablesorter">
							<thead>
								<tr>
									<th width="15%">Score</th>
									<th width="30%">Time Taken</th>
									<th width="15%">Date</th>
								</tr>
							</thead>
							<tbody>
							<%
								List<Activity> pasts = activityManager.getRecentActivityForUserAndQuiz(user.getId(), quiz.getId());
								for (Activity activity: pasts) {
							%>
								<tr class="hover-highlight">
										<td><%= roundTwoDecimals(activity.getScore() * (double)100) %>%</td>
										<td><%= roundTwoDecimals(activity.getTimeTaken() / (double)1000) %> sec</td>
										<td><%= activity.getTimestamp() %></td>
								</tr>
							<%
								}
							%>
							</tbody>
						</table>
				  	</li>
				  	
				  	<!-- USERS' BEST RESULTS -->
				  	<li id="topTab" class="tablesorter">
						<br/>
							<table width="800" id="top-performance-table" >
								<thead>
									<tr>
										<th width="15%">Score</th>
										<th width="15%">User</td>
										<th width="30%">Time Taken</th>
									</tr>
								</thead>
								<tbody>
								<%
									List<Activity> tops = activityManager.getTopActivityForQuiz(quiz.getId());
									for (Activity activity: tops) {
								%>
									<tr class="hover-highlight">
											<td><%= roundTwoDecimals(activity.getScore() * (double)100) %>%</td>
											<td><a href="user.jsp?user_id=<%= activity.getUser_id() %>"><%= activity.getUsername() %></a></td>
											<td><%= roundTwoDecimals(activity.getTimeTaken() / (double)1000) %> sec</td>
									</tr>
								<%
									}
								%>
								</tbody>
							</table>
				  	</li>
				  	
				</ul>
				
			</div>
		</div>
		<div id="footer" class="row">
			Stanford University Winter 2012. Site powered by Google App Engine, built using Zurb Foundations, and JQuery.
		</div>
		
	</div>
	<!-- container -->

	<!-- Included JS Files -->
	<script src="javascripts/jquery.min.js"></script>
	<script src="javascripts/jquery.gritter.min.js"></script>
	<script src="javascripts/modernizr.foundation.js"></script>
	<script src="javascripts/foundation.js"></script>
	<script src="javascripts/app.js"></script>
	<script src="javascripts/main.js"></script>

</body>
</html>
