<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*, com.backend.*, com.models.*, com.models.questions.*, java.text.DecimalFormat" %>
<%! @SuppressWarnings("unchecked") %>
<%!
	double roundTwoDecimals(double d) {
	    DecimalFormat twoDForm = new DecimalFormat("#.#");
		return Double.valueOf(twoDForm.format(d));
	}
%>
<%

	/* 
	* Bounced here from QuizServlet;
	* a transparent layer between the QuizServlet
	* and quiz.jsp, that in multi-page mode
	* just takes in a question parameter and a 
	* a list of user answers and tells the user 
	* whether the answer was right or wrong,
	* and displays the user's current score.
	*
	*/
	

	if(session.getAttribute("curQuestion") == null ||
			session.getAttribute("userAnswers") == null) {
			request.getRequestDispatcher("quiz.jsp").forward(request, response);
		return;
	}

	Quiz currentQuiz = (Quiz)session.getAttribute("quiz");
	
	// @PARAM questionList : the list of questions we're iterating
	// through
	List<BaseQuestion> questionList;
	if(session.getAttribute("questions") instanceof List) { 
		questionList = (List<BaseQuestion>) session.getAttribute("questions");
	} 
	else {
		// just forward to the next page without giving feedback.
		request.getRequestDispatcher("quiz.jsp").forward(request, response);
		return;
	}
	
	int curQuestion = ((Integer) session.getAttribute("curQuestion")).intValue();
	BaseQuestion q = questionList.get(curQuestion - 1);
	List<String> userAnswers = (List<String>) session.getAttribute("userAnswers"); 
	
	boolean correct = (q.checkAnswer(userAnswers) > 0);
	
	// score calculations
	
	int curScore = ((Integer) session.getAttribute("curScore")).intValue();
	double finalScore = (double)curScore / (double)(q.points());
	double finalPercentage = roundTwoDecimals(finalScore * (double)100);
%>  

<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Welcome to Foundation</title>
  
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="stylesheets/foundation.css">
	<link rel="stylesheet" href="stylesheets/app.css">
	<link rel="stylesheet" href="stylesheets/reveal.css">
	<script src="javascripts/jquery.min.js" type="text/javascript"></script>
	<script src="javascripts/jquery.reveal.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="stylesheets/ie.css">
	<![endif]-->

	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body style="background-color:#323232;">
	<!-- container -->
	<div class="container">
		<!-- Modals Section -->
		<div id="ExitModal" class="reveal-modal">
			<h5>Are you sure you want to exit this quiz?</h5>
			<p>Exiting will cause you to lose all data, including progress, question, and more.</p>
			<!-- Don't know why but can't click properly on the link, so must use Javascript -->
			<a href="main.jsp" class="nice radius red button" data-reveal-id="myModal" data-animation="none" style="margin-top:30px" 
				onclick="self.location='main.jsp'">Exit</a>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		
		<div id="HintModal" class="reveal-modal">
			<h5>Choose Your Hint Kind (you can only choose one per quiz!)</h5>
			<div class="three columns">
				<div class="panel"><img src="images/icons/24-gift.png"></div>
				<div class="panel"><img src="images/icons/27-planet.png"></div>
				<div class="panel"><img src="images/icons/30-key.png"></div>
			</div>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		
		<div id="CompetitionModal" class="reveal-modal">
			<h5>Question Statistics: </h5>
			<h5>Current Competitors: </h5>
			<h5>Top Score: </h5>
			<h5>Friends Competing: </h5>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<!-- End of Modals Section -->
	
		<!-- Heading -->
		<div class="row" style="padding-top:30px;">
			
			<div id="quiz-container" class="nine columns drop-shadow padded">
					<h3><%= currentQuiz.getName() %></h3>
					<br/>
					<div class="row">
						<div class="twelve columns">
							<!-- the sidebar -->
							<div id="sidebar">
								<a data-reveal-id="ExitModal" data-animation="fade" title="Go back"><img src="images/icons/01-refresh.png" /><br/></a>
								<a data-reveal-id="HintModal" data-animation="fade" title="Use a Hint"><img src="images/icons/42-photos.png" /><br/></a>
								<a data-reveal-id="CompetitionModal" data-animation="fade" title=""><img src="images/icons/145-persondot.png" /><br/></a>
							</div>
							
							<form id="quizform" action="quiz.jsp" method="get">
							
							<!-- /////////////// The Question(s) ////////////////////// -->
							<% 
								/*
								* Give feedback 
								*/
								if(correct) {		
							%>
								<div> Correct! </div>
							<% } else { %>
								<div> Incorrect. </div>
							<% } %>
							<div>
								<h6>Your current score: <%= curScore %>/<%= q.points() %> (<%= finalPercentage %> %) </h6>
							</div>
							<input type="submit" class="small blue button" value="Next&#0187;&#0187;" />
							</form>
							<!-- /////////////// End of Question(s) ////////////////////// -->
						</div><!-- end of twelve columns -->
				</div><!-- column -->
			</div><!-- row -->
		</div><!-- quiz container -->
	</div><!-- container -->

	<!-- Included JS Files -->
	<script src="javascripts/jquery.min.js"></script>
	<script src="javascripts/modernizr.foundation.js"></script>
	<script src="javascripts/foundation.js"></script>
	<script src="javascripts/app.js"></script>
	<script src="javascripts/main.js"></script>
	<script src="javascripts/quiz.js"></script>
	<script src="javascripts/drag-n-drop.js"></script>
	<script src="javascripts/core.js"></script>

</body>
</html>
