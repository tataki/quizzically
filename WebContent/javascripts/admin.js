/* --------------------------
 * Admin interface GUI JS
 * Team Mach One 
 * Stanford CS 108 
 ------------------------*/

$(document).ready(function() {

	/* Admin Globals -----------
		Various globals; assume
		that we have window.MACH 
		defined.
	 --------------------------- */
	if(window.Mach != null) {
		$.extend(true, window.Mach, {
			optionsArray:[
						"remove-user",
						"promote-user",
						"delete-user",
						],
			sendMessageButton:"send-message-button",
			sendMessageForm:"send-message-form",
			userAdminServer:"UserAdminServlet",
		});
	
	} else return;
	
	adminInit();
	
});

/* 
* Initializes various UI aspects of admin interface
*/
function adminInit() {
	
	/* Send Message Code ---------------------------
	 * Sending messages from admin to user
	 ---------------------------------------------- */
	$('.' + Mach.sendMessageButton).submit(function() {
		$.post(Mach.messageServer, $('.' + Mach.sendMessageForm).serialize());
	});	
	$('.send-message').click(function() {
		
		var userid = $(this).parent().siblings(".options-user-id").html();
		console.log(userid);
		$('#toUser_id').attr("value", userid);
	});

/*	console.log($('.delete-announcement')); */

	$('.delete-announcement').click(function() {
		var announcement_id = $(this).parent().parent().attr("id");
		console.log(announcement_id);
		$.post(Mach.userAdminServer, {announcement_id:announcement_id, type:"delete-announcement"}, 
			function(data) {
				/* do nothing */
			});
	});
	
	$('.delete-quiz').click(function() {
		var quiz_id = $(this).attr("id");
		var that = this;
		$.post(Mach.userAdminServer, {quiz_id:quiz_id, type:"delete-quiz"}, 
			function(data) {
				if(data == "success") {
					$(that).remove();
					window.location.reload();
				}
				
			});
	});
	
	assignClickables();
	
	
	/* Manage Users code --------------------------
	 * Controls the code for managing users 
	 * post application
	 ---------------------------------------------- */
	/*	
	for(var i =0; i < Mach.optionsArray.length;i++) {
		// send a POST request to let the system know
		// what we want to do with that person.
		$("." + Mach.optionsArray[i]).click(function() {
			
			// generateGrowl("as;kdfjlfj", "Message", false, 3000);
			var opt = $(this).attr("class");
			
			var userid = $(this).parent().siblings(".options-user-id").html();
			// find our user id
			var spinnerPtr = $('<img class="spinner" src="images/spinner.gif" />');
			$(spinnerPtr).insertAfter( $(find_parent_who_has_class(this, "nav-bar")).toggle(false));
			
			var thisPtr = this; // save a pointer to this scope
			console.log(Mach.adminServer);
		
			$.ajax({
				url: Mach.adminServer,
				type: "POST",
				data: { type:"user", targetID:userid, option:opt },
				success: function(data) {
						$(spinnerPtr).remove();
						//
						// if we've clicked on remove user, on success, we destroy
						// this user-row
						//
						if($(thisPtr).attr("class") == "remove-user") {
							var toRemove = find_parent_who_has_class(thisPtr, "user-row");
							$(toRemove).remove();
						} 
						else 
							$(find_parent_who_has_class(thisPtr, "nav-bar")).toggle(true);
					},
				error: function(data) {
					$(spinnerPtr).remove();
					$(find_parent_who_has_class(thisPtr, "nav-bar")).toggle(true);
				}
			});
			
			//$.post(Mach.adminServer, { type:"user", user:userid, option:opt }, 
		});
	}*/
	
}
	

function assignClickables() {
	/* Our new way of managing users; just have button clicks with
	 * specific classes. These classes are :
	 * "delete-user", "make-admin", "remove-admin" */
	$('.delete-user').unbind("click").click(function() {
		var target_id = $(this).parent().parent().attr("id");
		var thisPtr = this;
		$.post(Mach.userAdminServer, {user_id:target_id, type:"delete-user"}, 
			function(data) {
				console.log(data);
				/*
				* Remove this table row on success. 
				* Pop up a growl alert to the user.
				*/
				if(data == "success") {
					var originalUserName= $(thisPtr).parent().siblings().children('.userid').html();
					generateGrowl("Success", "user " + originalUserName + " has been deleted", false, 3000);
					$(thisPtr).parent().parent().fadeOut('slow', function() { 
							$(thisPtr).parent().parent().remove();
					});
					$(thisPtr).parent().remove();
					$(thisPtr).remove();
				}
				assignClickables();
			});
	});
	
	$('.make-admin').unbind("click").click(function() {
		var target_id = $(this).parent().parent().attr("id");
		var thisPtr = this;
		$.post(Mach.userAdminServer, {user_id:target_id, type:"make-admin"}, 
			function(data) {
				console.log(data);
				/*
				* Toggle the admin field and alert the user with a message
				*/
				if(data == "success") {
					var originalUserName= $(thisPtr).parent().siblings().children('.userid').html();
					generateGrowl("Success", "user " + originalUserName + " has been made an admin", false, 3000);
					$(thisPtr).parent().children(".make-admin").attr("class", "remove-admin clickable").html("Remove Admin");
				}
				assignClickables();
		});	
	});
	
	$('.remove-admin').unbind("click").click(function() {
		var target_id = $(this).parent().parent().attr("id");
		var thisPtr = this;
		$.post(Mach.userAdminServer, {user_id:target_id, type:"remove-admin"}, 
			function(data) {
				console.log(data);
				/*
				* Toggle the admin field and alert the user with a message
				*/
				if(data == "success") {
					var originalUserName= $(thisPtr).parent().siblings().children('.userid').html();
					generateGrowl("Success", "user " + originalUserName + " has has their admin privileges taken away", false, 3000);
					$(thisPtr).parent().children(".remove-admin").attr("class", "make-admin clickable").html("Make Admin");
				}
				assignClickables();
			});
	});
}
