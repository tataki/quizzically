<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%

Cookie cookie = null;
Cookie[] cookies = request.getCookies();
if( cookies != null ){
	for (int i = 0; i < cookies.length; i++) {
		cookie = cookies[i];
		if(cookie.getName().equals("logged_in")) {
			Cookie newCookie = new Cookie("logged_in", "logged_in");
			newCookie.setValue("");
			newCookie.setMaxAge(0);
			response.addCookie(newCookie);
		}
  	}
}

session.setAttribute("user", null);
response.sendRedirect("login.jsp");

%>
</body>
</html>