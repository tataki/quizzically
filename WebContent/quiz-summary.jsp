<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*, com.backend.*, com.models.*, java.text.DecimalFormat" %>
<!DOCTYPE html>
<%!
	double roundTwoDecimals(double d) {
	    DecimalFormat twoDForm = new DecimalFormat("#.#");
		return Double.valueOf(twoDForm.format(d));
	}
%>

<%
	/*
	* Gets the quiz id from the requests parameters 
	* and fetches the quiz information as well as creator
	* info
	*/
	
	/*
	* In public view mode, we don't allow the user to take a quiz; instead a login button
	* is presented. We also don't allow the user to see user statistics; only the big three displayed statistics
	*/
	boolean publicView = false;
	User visitingUser = (User) request.getSession().getAttribute("user");
	Achievement a = null;
	if (visitingUser == null) {
		publicView = true;
	}
	else {
		// Check achievements
		QuizManager qm = (QuizManager) request.getServletContext().getAttribute("quizManager");
		int numQuizzes = qm.getNumOfQuizzesByUserId(visitingUser.getId());
		if (numQuizzes == 1) {
			a = Achievement.AMATEUR_AUTHOR(visitingUser.getId());
			if (!a.exists())
				a.upload();
			else
				a = null;
		}
		else if (numQuizzes == 5) {
			a = Achievement.PROLIFIC_AUTHOR(visitingUser.getId());
			if (!a.exists())
				a.upload();
			else
				a = null;
		}
		else if (numQuizzes == 10) {
			a = Achievement.PRODIGIOUS_AUTHOR(visitingUser.getId());
			if (!a.exists())
				a.upload();
			else
				a = null;
		}
	}
	
	if(request.getParameter("quiz_id") == null) {
		response.sendRedirect("404.html");
		return;
	}
	int quizId = Integer.parseInt(request.getParameter("quiz_id"));
	Quiz quiz = null;
	quiz = Quiz.fetch(quizId);
	if(quiz == null) {
		response.sendRedirect("404.html");
		return;
	}
	User creator = new User(quiz.getCreator_id());
	ActivityManager am = (ActivityManager) request.getServletContext().getAttribute("activityManager");
%>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Welcome to Foundation</title>
  
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="stylesheets/foundation.css">
	<link rel="stylesheet" href="stylesheets/app.css">
	<link rel="stylesheet" href="stylesheets/reveal.css">
	<script src="javascripts/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
	<script src="javascripts/jquery.reveal.js" type="text/javascript"></script>

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="stylesheets/ie.css">
	<![endif]-->


	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>

	<ul class="topbar" style="width:100%; height:25px;">
		<li><a href="main.jsp">Home</a></li>
		<li><a href="search.jsp">Search</a></li>
		<li><a href="LogoutServlet">Logout</a></li>
	</ul>
	
	<!-- container -->
	<div class="container">
		
		<!-- Heading -->
		<div class="row" style="padding-top:30px">
			<div class="twelve columns">
				<div class="six columns">
					<div><img src="images/quizzically-logo-small.png" /></div>
					<p>Powered by Team Mach-One, cs108, Stanford University</p>
				</div>
				
				<div class="six columns">
					<p style="padding-top:80px; padding-left:180px;">
						
					</p>
				</div>
			</div>
		<hr />
		</div>
		<!-- End Heading -->
		
		<%
			if (a != null) {
		%>
				<div style="display:none;" class="achievement">
					<h6>You've earned an achievement!</h6>
					<p><%=a.getAward()%></p>
				</div>
		<%
			}
		%>
		
		<% if (!publicView) { %>
			<div id="create-Message-Modal" class="reveal-modal">
				<h5>Send Challenge</h5>
				<form id="challenge" action="MessageServlet" method="post">
					<h6>Send to friend:</h6>
					<input type="hidden" name="compose" value="1" />
					<input type="hidden" name="shouldRedirect" value="1" />
					<input type="hidden" name="quiz_id" value="<%= quiz.getId() %>" /> 
					<input type="text" name="toUser_id" placeholder="Username" />
					<input type="hidden" name="type" value="challenge" />
					<input class="small green button" type="submit" value="Challenge!" />  
				</form>
				<a class="close-reveal-modal">&#215;</a>
			</div>
		<% } %>
		
		
		<div class="row" style="min-width:1000px;">
			<div class="twelve columns">
				<h3><%= quiz.getName() %></h3>
				<div class="row">
					<div class="four columns" style="padding:20px; max-width:300px;">
					
						<!--
						////////////////////////////////////////////////////////////////////
						LINKS: 
						Sometimes clicking on buttons should logically take you to a 
						specific webpage to view a specific userid. 
						///////////////////////////////////////////////////////////////////
						
						Make sure that the servlet takes care of this by appending the proper
						id to whatever links necessitate them.
						 -->				
					
						<h5>By <a href="user.jsp?user_id=<%=creator.getId()%>" ><%=creator.getName()%></a><br/><br/>
							<% if(publicView) { %>
								
								<p>You cannot take this quiz because you are not signed in!</p>
								<a class="small radius red button" href="login.jsp">Sign in to take</a>
							
							<% } else { %>
							
								<form action="QuizServlet" method="get">
									<input type="hidden" name="quiz_id" value="<%=quiz.getId()%>" />
									<input type="submit" value="Take!" title="action=view" class="radius button" />
								</form>
								<form action="QuizServlet" method="get">
									<input type="hidden" name="quiz_id" value="<%=quiz.getId()%>" />
									<input type="hidden" name="practice" value="yes" />
									<input type="submit" value="Practice!" title="action=view" class="radius button" />
								</form>
								<a class="radius red button" data-reveal-id="create-Message-Modal" data-animation="none">Send Challenge</a>
							
							<% } %>
						</h5>
						<br/>

					</div>
					<a class="columns gray box bluehover-highlight" style="min-width:175px; min-height:85px; width:175px; height:85px; overflow:hidden"><%= am.getQuizCountTaken(quiz.getId()) %><br/><br/><h6>Taken</h6></a>
					<a class="columns blue box hover-highlight" style="width:175px; height:85px; overflow:hidden"><%= roundTwoDecimals(am.getAverageScoreQuiz(quiz.getId()) * (double)100) %>%<br/><br/><h6>Average Score</h6></a>
					<a class="columns pink box pinkhover-highlight" style="width:175px; height:85px;  float:left; overflow:hidden"><%= roundTwoDecimals(am.getTopScoreByQuizId(quiz.getId()) * (double)100) %>%<br/><br/><h6>Highest Score</h6></a>
				</div>
				
				<h6><%= quiz.getDescription() %></h6>
				<br/>
				Tags:<% 
				TagManager tm = new TagManager();
				for(Tag tag:tm.fetchTagsForQuiz(quizId)){ %>
						<a href="search.jsp?searchbar=<%=tag.getTag()%>" class="round tag" ><%="#"+tag.getTag()%></a>
						<% }%>
				<hr/>
				<br/>
				<br/>
				
				
				<h5>Performance Comparison:</h5><br/>
				
				<!-- RESULTS TABS -->
				<dl class="contained tabs">
				  <dd><a href="#past" class="active">Your Past</a></dd>
				  <dd><a href="#top">Best Ever</a></dd>
				  <dd><a href="#last">Most Recent</a></dd>
				  <dd><a href="#lastDay">Last Day</a></dd>
				</dl>
				
				<!-- RESULTS TAB CONTAINER -->
				<ul class="nice tabs-content contained">
			
					<!-- YOUR OWN PAST RESULTS -->
					<li id="pastTab" class="active">
				  		<br/>
						<table width="800" id="user-performance-table" class="tablesorter">
									<thead>
										<tr>
											<th width="15%">Score</th>
											<th width="30%">Time Taken</th>
											<th width="15%">Date</th>
										</tr>
									</thead>
									<tbody>
									<%
										List<Activity> pasts;
										if (visitingUser != null)
											pasts = am.getRecentActivityForUserAndQuiz(visitingUser.getId(), quiz.getId());
										else
											pasts = new ArrayList<Activity>();
										for (Activity activity: pasts) {
									%>
										<tr class="hover-highlight">
												<td><%= roundTwoDecimals(activity.getScore() * (double)100) %>%</td>
												<td><%= roundTwoDecimals(activity.getTimeTaken() / (double)1000) %> sec</td>
												<td><%= activity.getTimestamp() %></td>
										</tr>
									<%
										}
									%>
									</tbody>
						</table>
				  	</li>
				  	
				  	<!-- USERS' BEST RESULTS -->
				  	<li id="topTab">
						<br/>
							<table width="800"  id="top-performance-table" class="tablesorter">
								<thead>
									<tr>
										<th width="15%">Score</th>
										<th width="15%">User</td>
										<th width="30%">Time Taken</th>
									</tr>
								</thead>
								<tbody>
								<%
									List<Activity> tops;
									if (visitingUser != null)
										tops = am.getTopActivityForQuiz(quiz.getId());
									else
										tops = new ArrayList<Activity>();
									for (Activity activity: tops) {
								%>
									<tr class="hover-highlight">
											<td><%= roundTwoDecimals(activity.getScore() * (double)100) %>%</td>
											<td><a href="user.jsp?user_id=<%= activity.getUser_id() %>"><%= activity.getUsername() %></a></td>
											<td><%= roundTwoDecimals(activity.getTimeTaken() / (double)1000) %> sec</td>
									</tr>
								<%
									}
								%>
								</tbody>
							</table>
				  	</li>
				  	
				  	<!-- MOST RECENT REGARDLESS OF RESULTS -->
					<li id="lastTab" >
				  		<br/>
						<table width="800" id="last-performance-table" class="tablesorter">
									<thead>
										<tr>
											<th width="15%">Score</th>
											<th width="15%">User</th>
											<th width="15%">Time Taken</th>
											<th width="15%">Date</th>
										</tr>
									</thead>
									<tbody>
										<%
											List<Activity> recentActivities;
											if (visitingUser != null)
												recentActivities = am.getRecentActivityByQuizId(quiz.getId());
											else
												recentActivities = new ArrayList<Activity>();
											for (Activity activity: recentActivities) {
										%>
											<tr class="hover-highlight">
													<td><%= roundTwoDecimals(activity.getScore() * (double)100) %>%</td>
													<td><a href="user.jsp?user_id=<%= activity.getUser_id() %>"><%= activity.getUsername() %></a></td>
													<td><%= roundTwoDecimals(activity.getTimeTaken() / (double)1000) %> sec</td>
													<td><%= activity.getTimestamp() %></td>
											</tr>
										<%
											}
										%>
									</tbody>
						</table>
				  	</li>
				  	
				  	<!-- TOP LAST DAY RESULTS -->
					<li id="lastDayTab" >
				  		<br/>
						<table width="800" id="top-last-day-performance-table" class="tablesorter">
									<thead>
										<tr>
											<th width="15%">Score</th>
											<th width="15%">User</th>
											<th width="15%">Time Taken</th>
											<th width="15%">Date</th>
										</tr>
									</thead>
									<tbody>
										<%
											List<Activity> lastDayActivities;
											if (visitingUser != null)
												lastDayActivities = am.getRecentActivityByQuizIdLastDay(quiz.getId());
											else
												lastDayActivities = new ArrayList<Activity>();
											for (Activity activity: lastDayActivities) {
										%>
											<tr class="hover-highlight">
													<td><%= roundTwoDecimals(activity.getScore() * (double)100) %>%</td>
													<td><a href="user.jsp?user_id=<%= activity.getUser_id() %>"><%= activity.getUsername() %></a></td>
													<td><%= roundTwoDecimals(activity.getTimeTaken() / (double)1000) %> sec</td>
													<td><%= activity.getTimestamp() %></td>
											</tr>
										<%
											}
										%>
									</tbody>
						</table>
				  	</li>
				</ul>
				
			</div>
		</div>
		<div id="footer" class="row">
			Stanford University Winter 2012. Site powered by Google App Engine, built using Zurb Foundations, and JQuery.
		</div>
		
	</div>
	<!-- container -->

	<!-- Included JS Files -->
	<script src="javascripts/jquery.min.js"></script>
	<script src="javascripts/modernizr.foundation.js"></script>
	<script src="javascripts/foundation.js"></script>
	<script src="javascripts/app.js"></script>
	<script src="javascripts/main.js"></script>
	<script src="javascripts/core.js"></script>
	<script src="javascripts/drag-n-drop.js"></script>
	<script src="javascripts/quiz.js"></script>
	<script src="javascripts/jquery.tablesorter.min.js"></script>
	<script src="javascripts/jquery.gritter.min.js"></script>
	
</body>
</html>
