<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*, com.backend.*, com.models.* " %>
    
<%
	if(session.getAttribute("special") == null) {	
		response.sendRedirect("login.jsp");
		return;
	}
	else if(!session.getAttribute("special").equals("29dd2f9f8d9312235caab2629e28ad45")) {
		response.sendRedirect("login.jsp");
		return;
	}
	/* TODO: DO SOME ERROR CHECKING FOR ADMIN PAGE; 
	 * we should never as an ordinary or outside user be able to reach here */
	
	User user = (User)request.getSession().getAttribute("user");
	List<User> userList = User.getUsersByFilter("%");

	if(user == null) {
	    response.sendRedirect("login.jsp");
	    return;
	}
	if(user.isAuthenticated() == false) {
	    response.sendRedirect("login.jsp");
	    return;
	}
	if(user.isAdmin() == false) {
	    response.sendRedirect("login.jsp");
	    return;		
	}

	ServletContext context = request.getServletContext();
	context.setAttribute("user", user);
 	AnnouncementManager announcementManager = (AnnouncementManager) context.getAttribute("announcementManager");
	List<Announcement> announcementArray = announcementManager.getAllAnnouncement();

	QuizManager quizManager = (QuizManager) context.getAttribute("quizManager");
	List<Quiz> quizArray = quizManager.getRecent();
%>    
    
<html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Welcome to Foundation</title>
  
	<!-- Included CSS Files -->
	<!-- <link rel="stylesheet" href="stylesheets/foundation.css"> -->
	<link rel="stylesheet" href="stylesheets/app.css">

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="stylesheets/ie.css">
	<![endif]-->


	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>

	<ul class="topbar" style="width:100%; height:25px;">
		<li>&nbsp;<span class="red label">Admin Mode</span></li>
		<li><a href="main.jsp">Home</a></li>
		<li><a href="search.jsp">Search</a></li>
		<li><a href="LogoutServlet">Logout</a></li>
	</ul>

	<!-- container -->
	<div class="container">

		<!--  Modals Section -->
		<div id="MessageModal" class="reveal-modal">
			<form id="sendMessage" action="MessageServlet" method="post">
				<h6>Send Message</h6>
				<input type="hidden" id="fromUser_id" name="fromUser_id" value="324" /><!--  Make sure to replace value here with admin user id -->
				<input type="hidden" id="toUser_id" name="toUser_id" value="1" />
				<input type="hidden" name="type" value="note" />
				<textarea class="expand" placeholder="Your Message" id="_message" name="message" rows="3"></textarea>
				<input class="small blue button" type="submit" value="Send!" />  
			</form>
		</div>
		<!--  Close Modals Section -->

		<!-- Heading -->
		<div class="row" style="padding-top:30px">
			<div class="twelve columns">
				
				<div class="six columns">
				
					<div><img src="images/quizzically-logo-small.png" /></div>
					<p>Powered by Team Mach-One, cs108, Stanford University</p>
				</div>
			</div>
		<hr />
		</div>
		
		
		<!-- End Heading -->
		
		<!-- TABS: SHOULD NOT HAVE TO CHANGE -->
		<div class="row">
			<div class="three columns" style="height:100%; border: solid 0 #ddd; border-width: 0px 1px 0px 0px;">
				<h5>Control Panel</h5>
				<dl class="nice vertical tabs">
				  <dd><a href="#announcements" class="active hover-light" style="font-size:15px;"><img class="icons" src="images/icons/28-star.png" />Announcements</a></dd>
				  <dd><a href="#manage-users" class="hover-light" style="font-size:15px;"><img class="icons" src="images/icons/111-user.png" />Manage Users</a></dd>
				  <dd><a href="#quizzes" class="hover-light" style="font-size:15px;"><img class="icons" src="images/icons/140-gradhat.png" />Quizzes</a></dd>
				  <dd><a href="#site-statistics" class="hover-light" style="font-size:15px;"><img class="icons" src="images/icons/123-id-card.png" />Site Statistics</a></dd>
				</dl>
			</div>
			
			<!-- Main Administrative Cotent -->
			<div class="six columns content" style="padding-left:20px;">
				<h5>Announcements</h5>
					<!-- Table Row representing a user -->
				<ul class="tabs-content contained">
				
					<!-- Annnouncements Tab Content -->
					 <li class="active" id="announcementsTab">
					 <div>
							<h5>Make Announcements</h5>
							 <!-- Make an announcement -->
							 <form action="UserAdminServlet" method="post"><br/>
								<input type="hidden" name="type" value="create-announcement" />
								<textarea class="expand" placeholder="Attention!" id="announcement" name="announcement" rows="3"></textarea>
								<select name="importance">
									<option value="1">Low</option>
									<option value="2">Medium</option>
									<option value="3">High</option>
								</select><br/>
								<input type="submit" class="blue button" value="Announce!" />
							 </form>
						 </div>
						 <hr/>
						 <br/>
					 	<div style="margin-bottom:10px;">
					 	<h5>Recent Announcements</h5><br/>
							 <!-- Previous announcements -->
   							 <ul id="announcement-container">
								<%
								Announcement.Importance localAnnouncementImportance;
								String localAnnouncement;
								if(announcementArray.isEmpty() == false) 
								{
									for (int i = 0; i < announcementArray.size(); i++) 
									{
										Announcement announcement = announcementArray.get(i);
										localAnnouncement = announcement.getText();
										localAnnouncementImportance = announcement.getImportance(); %>
										<li class="announcement" id="<%= announcement.getId()%>">
											<div class="alert-box <%=localAnnouncementImportance%>"><%=localAnnouncement%><a href="" class="delete-announcement close">&times;</a></div>
										</li>
								<% 
									} 
								} else {
									localAnnouncement = "No announcements!";
									localAnnouncementImportance = Announcement.Importance.LOW;
								%>
									<%= localAnnouncement %>
								<% } %>
							</ul>

<!--
   							 <ul id="announcement-container">
								<li class="announcement" id="333333"><div class="alert-box success">Here is an announcement. <br/><a href="" class="close">&times;</a>
									Quiz changes are coming! We're happy to announce that we're bringing a social component to the whole site.
									In addition, we're going to revamp our messaging and challenge system so that you can better 
								</div></li>
								<li class="announcement" id="444444"><div class="alert-box">Here is a less important announcement<a href="" class="close">&times;</a></div></li>
								<li class="announcement" id="555555"><div class="alert-box success">Here is an announcement.<br/> <a href="" class="close">&times;</a></div></li>
								<li class="announcement" id="6666"><div class="alert-box error">Here is an important announcement. Your account may have been hacked into
									by an anonymous third party. Please verify your information by going to the authentication pary of our wsebsite now.<a href="" class="close">&times;</a></div></li>
								<li class="announcement" id="8888"><div class="alert-box success" >Here is an announcement<a href="" class="close">&times;</a></div></li>
							</ul> -->
						</div>
					 </li>
					 
					 <!-- User Tab Content -->
					 <li id="manage-usersTab">
					 	<!-- a typical user -->
					 	<table width="600">
							<thead>
								<tr id="quizID">
									<th width="15%">UserName</th>
									<th width="25%">Email</th>
									<th width="30%">Admin</th>
									<th align="right"></th>
								</tr>
							</thead>
							<tbody>
								<!-- Table Row representing a user -->
								<%
								for(int i=0; i<userList.size(); i++) {
									User thisUser = userList.get(i);
									if(thisUser.getId() == user.getId()) continue;
									String userName = thisUser.getName(); %>
									
									<tr class="user-row" id="<%= thisUser.getId() %>">
										<td class="hover-highlight"><a class="userid" style="font-weight:bold;" href="user.jsp?user_id=<%=thisUser.getId()%>"><%=userName%></a></td>
										<td><%= thisUser.getEmail() %></td>
										<td>
										    <% if(thisUser.isAdmin() == true) { 
										    	
										    	%>
										    	<a class="remove-admin clickable">Remove Admin</a>
										   	<% }
										    else {
										    	session.setAttribute("user-admin", "make admin");
										    	%>
										   		<a class="make-admin clickable">Make Admin</a>
										   	<% } %>
										</td>
										<td align="right">
											<a class="delete-user clickable"><img src="images/delete.png" /></a>
										</td>
									</tr>
								<% }  %>
								
							</tbody>
				</table>
					 </li>
					 
					 <!-- Quiz Tab Content -->
					 <li id="quizzesTab">Quizzes content goes here
					 	<table width="600">
							<thead>
								<tr id="quizID">
									<th width="10%">Creator</th>
									<th width="15%">Name</th>
									<th width="40%">Description</th>
									<th width="25%">Created</th>
									<th width="15%"></th>
								</tr>
							</thead>
							<tbody>
								<!-- Table Row representing a user -->
								<%
								for(int i=0; i<quizArray.size(); i++) {
									Quiz thisQuiz = quizArray.get(i);
									User quizCreator = new User(thisQuiz.getCreator_id());
									String quizCreatorUser = quizCreator.getName();  
									String quizName = thisQuiz.getName();
									String quizDescription = thisQuiz.getDescription();
									String quizTimeStamp = thisQuiz.getTimestamp().toString();
									%>
									<tr class="user-row" id="<%=thisQuiz.getId() %>">
<!-- http://localhost:8080/Quizzically/quiz-summary.jsp?quiz_id=11 -->
 
									<td class="hover-highlight"><a class="userid" style="font-weight:bold;" href="user.jsp?user_id=<%=quizCreator.getId()%>"><%=quizCreator.getName() %></a></td>
									<td class="hover-highlight"><a class="quizid" style="font-weight:bold;" href="quiz-summary.jsp?quiz_id=<%=thisQuiz.getId()%>"><%=thisQuiz.getName() %></a></td>
									<td><%=quizDescription %></td>
									<td><%=quizTimeStamp %></td>
									<td align="right">
										<a class="delete-quiz clickable" id="<%= thisQuiz.getId() %>"><img src="images/delete.png" /></a>
									</td>
									</tr>
								<% }  %>
								
							</tbody>
				</table>
					 </li>
					 
					 <!-- Statistics Tab Content -->
					 <li id="site-statisticsTab">
						 <h5>Number of quizes: <%=quizArray.size() %></h5>
						 <h5>Number of users: <%=userList.size() %></h5>
					 </li>
				</ul>
			</div>
			<!-- End administrative content -->
			<div style="clear: both;"></div>
		</div>
		<div id="footer" class="row">
			Stanford University Winter 2012. Site powered by Google App Engine, built using Zurb Foundations, and JQuery.

	</div>
	<!-- container -->
	</div>



	<!-- Included JS Files -->
	<script src="javascripts/jquery.min.js"></script>
	<script src="javascripts/modernizr.foundation.js"></script>
	<script src="javascripts/foundation.js"></script>
	<script src="javascripts/app.js"></script>
	<script src="javascripts/main.js"></script>
	<script src="javascripts/admin.js"></script>

</body>
</html>
