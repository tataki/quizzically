<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.backend.*, com.models.*, java.util.* " %>
<%!
	/* computes whether target
	* is "recent", i.e. within
	* a certain amount of days as
	* determined by this function 
	*/
	
	public boolean timeDiffToday(Date target) {
		Date today = new Date();
		int numDays = 2;
		return ((today.getTime() - target.getTime()) * (long)1000) > (long)(60 * 60 * 24 * numDays);
	}
%>

<%
	/* TODO: load two search result lists in;
	* Right now list doesn't do anything. */
	
	User curUser = (User)session.getAttribute("user");
	if(curUser == null) {
		response.sendRedirect("login.jsp");
		return;
	}
	
	int id = curUser.getId();
	
	List<User> userResults = new ArrayList<User>();
	Set<User> friendSet = new HashSet<User>();
	Set<User> recommendationSet = new HashSet<User>();
	List<Quiz> quizResults = new ArrayList<Quiz>();
	TagManager tm = new TagManager();
	FriendRecommendation fr = new FriendRecommendation();
	String filter = request.getParameter("searchbar"); 

	
	if(filter!=null){
		//quiz part		
		QuizManager qm = new QuizManager();
		quizResults.addAll(qm.getRelevantQuizzes(filter));
		
		//user part
		//getRecommendation,getFriend ,getUser
		FriendManager fm = new FriendManager();
		List<User> userGivenRecommendation = fm.getRecommendation(id,filter);
		recommendationSet.addAll(userGivenRecommendation);
		List<User> userGivenFriendName = fm.getFriends(id,filter);
		friendSet.addAll(userGivenFriendName);
		List<User> userGivenUserName = User.getUsersByFilter(filter);
		Set<User> addedUser = new HashSet<User>(userGivenRecommendation);
		
		userResults.addAll(userGivenRecommendation);
		for(User user : userGivenFriendName){
			if(!addedUser.contains(user)){
				userResults.add(user);
				addedUser.add(user);
			}				
		}
		for(User user : userGivenUserName){
			if(!addedUser.contains(user)){
				userResults.add(user);
				addedUser.add(user);
			}				
		}
		
	}
%>    
    
<!DOCTYPE html">
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Welcome to Foundation</title>
  
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="stylesheets/app.css">

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="stylesheets/ie.css">
	<![endif]-->


	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>

	<ul class="topbar" style="width:100%; height:25px;">
		<li><a href="main.jsp">Home</a></li>
		<li><a href="search.jsp">Search</a></li>
		<li><a href="LogoutServlet">Logout</a></li>
	</ul>
	
	<!-- container -->
	<div class="container">
		
		<!-- Heading -->
		<div class="row" style="padding-top:30px">
			<div class="twelve columns">
				<div class="five columns">
					<div><img src="images/quizzically-logo-small.png" /></div>
					<p>Powered by Team Mach-One, cs108, Stanford University</p>
				</div>
				<div class="eight columns">
				<Form action="search.jsp" method="get">
						<input class="round" id="searchbar" type="search" size="50" name="searchbar" placeholder="Search for friends, quizzes, and more" />
					</Form> 
				</div>
			</div>
		<hr />
		</div>
		<!-- End Heading -->
		
	
		<div class="row">
			<div class="twelve columns">
			<!-- TABS: SHOULD NOT HAVE TO CHANGE -->
			<div class="two columns" style="height:100%; border: solid 0 #ddd; border-width: 0px 1px 0px 0px;">
				<h5>Filter By</h5>
				<dl class="nice vertical tabs">
				  <dd><a href="#nice0" class="active">Quiz</a></dd>
				  <dd><a href="#nice1">Users</a></dd>
				</dl>
			</div>
			
			<div class="ten columns content" style="min-width:300px;">
			<!-- TAB CONTENT: MODIFY AS NEEDED --->
			<ul class="nice tabs-content contained">
			  <!-- Quiz results 
			  	Fill this <li> tag with quiz results sorted by rating / recommended.
			  -->
			  <li class="active" id="nice0Tab">
			  	<% for (Quiz q : quizResults) { %>
			  	<div class="quizresult row twelve columns">
			  		<% if (timeDiffToday(q.getTimestamp())) { %>
			  			<span class="red label">Recently Created</span>
			  		<% } %> 
			  		<h6 class="title">&nbsp;&nbsp;<a href="quiz-summary.jsp?quiz_id=<%=q.getId()%>"><%= q.getName() %></a></h6>
			  		<h6>Created by <a href="user.jsp?user_id=<%= q.getCreator_id() %>"><%= q.getCreator_name() %></a></h6>
			  		<h6>Description: <%= q.getDescription() %></h6>
			  		<h6>Tags
			  		
			  		<% for(Tag tag:tm.fetchTagsForQuiz(q.getId())){ %>
						<a class="round tag"  href = "search.jsp?searchbar=<%=tag.getTag()%>" ><%="#"+tag.getTag()%></a>
						<% }%>
			  		</h6>
			  	</div>
			  	<% } %>
			  	
			  	
			  </li>
			
			  <!-- User results
			  	Fill this <li> tag with tag results.
			  -->
			  <li id="nice1Tab">
			  	<% for(User user : userResults) {%>
			  	<div class="quizresult row twelve columns">
			  		<h6 class="title"><a href="user.jsp?user_id=<%= user.getId() %>"><% if(friendSet.contains(user)){%> <span class="green label">Friend</span><%};%>
			  		&nbsp;&nbsp;<%= user.getName() %></a>
			  		<%if(recommendationSet.contains(user)){ %><small>  shares <%=fr.getNumFriends(user.getId(),id) %> friend(s) with you</small><% }%></h6>			  		
			  		<h6>Username: <a href="user.jsp?user_id=<%= user.getId() %>"><%= user.getName() %></a></h6>
			  		<h6><%= user.getEmail() %></h6>
			  		<h6>Privileges: <%= user.isAdmin() ? "Admin" : "None" %></h6>
			  	</div>
			  	<% } %>

			  </li>
			  
			</ul>  <!-- END OF TAB CONTENT -->
			</div> <!-- end of six column container -->
		</div> <!-- end of twelve column container -->
		</div>
	</div>
		<div id="footer" class="row">
			Stanford University Winter 2012. Site powered by Google App Engine, built using Zurb Foundations, and JQuery.

	</div>
	<!-- container -->




	<!-- Included JS Files -->
	<script src="javascripts/jquery.min.js"></script>
	<script src="javascripts/modernizr.foundation.js"></script>
	<script src="javascripts/foundation.js"></script>
	<script src="javascripts/app.js"></script>
	<script src="javascripts/main.js"></script>

</body>
</html>
