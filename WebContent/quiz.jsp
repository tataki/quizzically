<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*, com.backend.*, com.models.*, com.models.questions.* " %>
<%! @SuppressWarnings("unchecked") %>
<%

	/* Initial Page Setup 
	* When taking the quiz, we find the current question.
	* Depending on the question type, we print out
	* a certain section of this JSP file that corresponds
	* to the question type. 
	*/
	
	Quiz currentQuiz = (Quiz)session.getAttribute("quiz");
	
	// @PARAM questionList : the list of questions we're iterating
	// through
	List<BaseQuestion> questionList;
	if(session.getAttribute("questions") instanceof List) { 
		questionList = (List<BaseQuestion>) session.getAttribute("questions");
	} 
	else {
		// die here 
		response.sendRedirect("404.html");
		return;
	}
	
	// @PARAMS : isMultiplePage, curQuestioNum
	// (only get the curQuestion if multiple pages)
	boolean isMultiplePage = true;
	int curQuestionNum = -1;
	
	if(session.getAttribute("curQuestion") == null)
		isMultiplePage = false;

	if(isMultiplePage) {
		curQuestionNum = (Integer) session.getAttribute("curQuestion");
		if(curQuestionNum > questionList.size()) {
			// we have an out-of-bounds current question number
			response.sendRedirect("404.html");
			return;
		}
	}
%>  

<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Welcome to Foundation</title>
  
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="stylesheets/foundation.css">
	<link rel="stylesheet" href="stylesheets/app.css">
	<link rel="stylesheet" href="stylesheets/reveal.css">
	<script src="javascripts/jquery.min.js" type="text/javascript"></script>
	<script src="javascripts/jquery.reveal.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="stylesheets/ie.css">
	<![endif]-->

	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body style="background-color:#323232;">
	<!-- container -->
	<div class="container">
		<!-- Modals Section -->
		<div id="ExitModal" class="reveal-modal">
			<h5>Are you sure you want to exit this quiz?</h5>
			<p>Exiting will cause you to lose all data, including progress, question, and more.</p>
			<!-- Don't know why but can't click properly on the link, so must use Javascript -->
			<a href="main.html" class="nice radius red button" data-reveal-id="myModal" data-animation="none" style="margin-top:30px" 
				onclick="self.location='main.jsp'">Exit</a>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		
		<div id="HintModal" class="reveal-modal">
			<h5>Choose Your Hint Kind (you can only choose one per quiz!)</h5>
			<div class="three columns">
				<div class="panel"><img src="images/icons/24-gift.png"></div>
				<div class="panel"><img src="images/icons/27-planet.png"></div>
				<div class="panel"><img src="images/icons/30-key.png"></div>
			</div>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		
		<div id="CompetitionModal" class="reveal-modal">
			<h5>Question Statistics: </h5>
			<h5>Current Competitors: </h5>
			<h5>Top Score: </h5>
			<h5>Friends Competing: </h5>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<!-- End of Modals Section -->
	
		<!-- Heading -->
		<div class="row" style="padding-top:30px;">
			
			<div id="quiz-container" class="nine columns drop-shadow padded">
					<h3><%= currentQuiz.getName() %></h3>
					<br/>
					<% if (isMultiplePage) { %>
						<h6>Points: <%= questionList.get(curQuestionNum).points() %></h6>
					<% } %>
					<div class="row">
						<div class="twelve columns">
							<!-- the sidebar -->
							<div id="sidebar">
								<a data-reveal-id="ExitModal" data-animation="fade" title="Go back"><img src="images/icons/01-refresh.png" /><br/></a>
								<a data-reveal-id="HintModal" data-animation="fade" title="Use a Hint"><img src="images/icons/42-photos.png" /><br/></a>
								<a data-reveal-id="CompetitionModal" data-animation="fade" title=""><img src="images/icons/145-persondot.png" /><br/></a>
							</div>
							
							<form id="quizform" action="QuizServlet" method="post">
							
							<!-- /////////////// The Question(s) ////////////////////// -->
							<% 
								
								/*
								* If single page, for loop through. If multiple pages, display question and return.
								*/
								
								int start = (isMultiplePage ? curQuestionNum : 0);
								int end = (isMultiplePage ? curQuestionNum + 1 : questionList.size());
								for(int i = start; i < end; i++)  {
									/* Upcast from BaseQuestion depending on the type; because we're downcasting,
									we're guaranteed that "question" retains its original type's properties, even
									if it has been casted to something different (i.e. it still retains its old
									references to overridden methods and private variables from when it was first created. 
									*/
									BaseQuestion question = questionList.get(i);
									if(question == null) break;
									String qname = (isMultiplePage ? "answer" : "answer-" + (i + 1)); // placeholder for names
									
							%>
								<%if (question.isTimed() && isMultiplePage) { /* the timer */ %>
									<div>When the timer reaches zero, you will be automatically redirected to the next question.</div>
									<div>Time:<span class="timer"><%= question.getTime() %></span></div><br/>
								<%  } %>
								
								<!--          Unique Question        -->
								<% 
									if (question.getType().equals(QuestionType.QuestionResponse) ||
										question.getType().equals(QuestionType.FillInTheBlank)) 
									{  
								%>
									
									 <!-- Question Response / Fill in the Blank -->
									<h4><%= question.getQuestion() %></h4>
									<br/>
									<input type="text" size="50" id="answer" name="<%= qname %>" placeholder="Your Answer" />
									<br/>
									<br/>
								<% 
										continue; // move one iteration further
									}
								%>
								
								<% if (question.getType().equals(QuestionType.MultiChoice)) { 
									MultipleChoiceQuestion mcq = (MultipleChoiceQuestion) question;
								%>
									 <!-- Multiple Choice -->
									<h4><%= question.getQuestion() %></h4>
									<br/>
									<% for (String ans : mcq.getChoices()) { %>
										<input type="radio" id="answer" name="<%= qname %>" value="<%= ans %>" placeholder="Your Answer" /><%= ans %><br/>
									<% } %>
									<br/>
									<br/>
								<% 
										continue; // move one iteration further
									}
								%>
								
								<% if (question.getType().equals(QuestionType.Matching)) { 
									MatchingQuestion matchQ = (MatchingQuestion) question;
								%>
									<!-- Matching : Draggable Container Div -->
									<h6 class="undo-matching"><a>[-] Undo Matching</a></h6>
									<div class="draggable" id="matchquestions">
										<h5 class="draggable-ignore questionBegin">Match these items:</h5>
										<!--
											The matching questions. Drag these on top of 
											answers to form a "match"  
										 -->
										<% 	
											Set<Integer> s = new HashSet<Integer>();
										
											int max = matchQ.getQuestions().size();
											
											for(int j =0; j < matchQ.getQuestions().size(); j++) {
												Random rand = new Random();
												int qindex = rand.nextInt(max);
												while(s.contains(qindex) && max != s.size()) {
													qindex = rand.nextInt(max);
												}
												s.add(qindex);
										%>
											<div class="question"><span class="name draggable-ignore">?</span><input type="text" disabled="disabled" name="<%= qname %>" class="info draggable-ignore" value="<%= matchQ.getAnswers().get(qindex) %>" /></div>
										<% 
											} 
										%>
										
										<br/><br/>
										<h5 class="draggable-ignore answerBegin">... to these items:</h5>
										<br/><br/>
										<!--
											The matching answers. Cannot be dragged.
											These are in order 
										 -->
										<% for(int j = 0; j < matchQ.getAnswers().size(); j++) { %>
											<div class="answer"><span class="name draggable-ignore "><%=j %></span><input type="text" disabled="disabled" class="info draggable-ignore" name="<%= j %>" value="<%= matchQ.getQuestions().get(j) %>" /></div>
										<% } %>
									</div>
									<br/><br/>
																	
									<!--  Hack : Record some hidden information that's modified during the drag-drop process -->
									<%
											for(int j = 0; j < matchQ.getAnswers().size(); j++) { 
									%>
												<!-- <input type="hidden" class="info draggable-ignore special-record" name="<%= qname %>" value="<%= matchQ.getAnswers().get(j) %>" /> -->
												<input type="hidden" class="info draggable-ignore special-record" name="<%= qname %>" value="<%= j %>" />
									<% 		}
										
									%>
									
								<%  
										continue; // move one iteration further
									}
								%>
								
								<% if (question.getType().equals(QuestionType.PictureResponse)) { 
									PictureResponseQuestion prq = (PictureResponseQuestion) question;
								%>
									<!-- Picture Response  -->
										<h4><%= question.getQuestion() %></h4>
										<br/>
										<br/>
										<img src="<%= prq.getImgUrl() %>" width="300px" />
										<br/>
										<br/>
										<input type="text" size="50" id="answer" name="<%= qname %>" placeholder="Your Answer" />
										<br/>
										<br/>
								<%  
										continue; // move one iteration further
									}
								%>
								
								<% if (question.getType().equals(QuestionType.MultiAnswer)) { 
									MultipleAnswersQuestion maq = (MultipleAnswersQuestion) question;
								%>
									<!-- Multiple Answers Response  -->
										<h4><%= question.getQuestion() %></h4>
										<br/>
										<% for (int j = 0 ; j < maq.getAnswers().size(); j++) { %>
											<input type="text" size="50" name="<%= qname %>" placeholder="Your Answer" /><br/>
										<% } %>
										<br/><br/>
								<%  
										continue; // move one iteration further
									}
								%>
								
								<% if (question.getType().equals(QuestionType.MultiChoiceMultiAnswer)) { 
									MultipleChoiceAndAnswersQuestion mcma = (MultipleChoiceAndAnswersQuestion) question;
								%>
									<!-- Multiple Choices and Answers Response  -->
										<h4><%= question.getQuestion() %></h4>
										<% for (int j = 0; j < mcma.getChoices().size(); j++) { %>
											<input type="checkbox" size="50" name="<%= qname %>" value="<%= mcma.getChoices().get(j) %>" />
											<%= mcma.getChoices().get(j) %><br/>
										<% } %>
										<br/><br/>
								<%  
										continue; // move one iteration further
									}
								%>
								
								<!--           End of Unique Question        -->
								
							<% 
								/* end of for loop */
								} 
							%>
							
							<%
								if (request.getParameter("practice") != null) { 
							%>
								<input type="hidden" name="practice" value="yes" />
							<% 	
								}
							%>
							<input type="submit" class="small blue button" value="Next&#0187;&#0187;" />
							</form>
							<!-- /////////////// End of Question(s) ////////////////////// -->
						</div><!-- end of twelve columns -->
				</div><!-- column -->
			</div><!-- row -->
		</div><!-- quiz container -->
	</div><!-- container -->

	<!-- Included JS Files -->
	<script src="javascripts/jquery.min.js"></script>
	<script src="javascripts/modernizr.foundation.js"></script>
	<script src="javascripts/foundation.js"></script>
	<script src="javascripts/app.js"></script>
	<script src="javascripts/main.js"></script>
	<script src="javascripts/quiz.js"></script>
	<script src="javascripts/drag-n-drop.js"></script>
	<script src="javascripts/core.js"></script>

</body>
</html>
