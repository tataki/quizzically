<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*, com.backend.*, com.models.*, java.text.DecimalFormat, java.math.* " %>
    
<!DOCTYPE html>
<%!
	double roundTwoDecimals(double d) {
	    DecimalFormat twoDForm = new DecimalFormat("#.#");
		return Double.valueOf(twoDForm.format(d));
	}
%>
<%
	/*
	* In public view mode, we don't allow the user to take a quiz; instead a login button
	* is presented. We also don't allow the user to see user statistics; only the big three displayed statistics
	*/
	boolean publicView = false;
	User visitingUser = (User) request.getSession().getAttribute("user");
	if(visitingUser == null) {
		publicView = true;
	}
	
	if(request.getParameter("user_id") == null) {
		response.sendRedirect("404.html");
		return;
	}
	int userId = Integer.parseInt(request.getParameter("user_id"));
	User user = new User(userId);
	
	/* Set up managers */
	ServletContext context = request.getServletContext();
	QuizManager quizManager = (QuizManager) context.getAttribute("quizManager");
	ActivityManager activityManager = (ActivityManager) context.getAttribute("activityManager");
	AchievementManager achievementManager = (AchievementManager) context.getAttribute("achievementManager");
	
	/* Get user tags */
	//List<Tag> tags =(List<Tag>) user.tagManager.fetchTagsForUser(user.getId());
	List<Tag> tags = new ArrayList<Tag>();
	/* Get recent activity  for this user*/
	List<Activity> activities = activityManager.getRecentActivity(user.getId());
	
	/* Get quizzes created */
	List<Quiz> quizzes = quizManager.getByUserId(user.getId());
%>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Welcome to Foundation</title>
  
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="stylesheets/app.css">
	<link rel="stylesheet" href="stylesheets/reveal.css">
	<script src="javascripts/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
	<script src="javascripts/jquery.reveal.js" type="text/javascript"></script>

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="stylesheets/ie.css">
	<![endif]-->


	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>

	<% if(!publicView) { %>
	<ul class="topbar" style="width:100%; height:25px;">
		<li><a href="main.jsp">Home</a></li>
		<li><a href="search.jsp">Search</a></li>
		<li><a href="LogoutServlet">Logout</a></li>
	</ul>
	<% } %>

	<!-- container -->
	<div class="container">
		
		<!-- Heading -->
		<div class="row" style="padding-top:30px">
			<div class="twelve columns">
				<div class="six columns">
					<div><img src="images/quizzically-logo-small.png" /></div>
					<p>Powered by Team Mach-One, cs108, Stanford University</p>
				</div>
				
				<div class="six columns">
					<p style="padding-top:80px; padding-left:180px;">
						
					</p>
				</div>
			</div>
		<hr />
		</div>
		<!-- End Heading -->
		
		
		<div class="row">
			<div class="twelve columns">
				<h3>
					<%= user.getUserName() %>
					
				</h3>
				<h6>Last quiz taken: <% List<Activity> recentActivities = activityManager.getRecentActivity(user.getId()); 
										if (recentActivities.isEmpty()) { %>
									 		Unknown 
									 	<% } else { %>
									 		<%= recentActivities.get(0).getTimestamp() %>
									 	<% } %></h6>
				<% if(!publicView) { %>
					<h6><a href="mailto:<%= user.getEmail() %>">Email: <%= user.getEmail()  %></a></h6>
				<% } %>
				<h6>
					
				</h6>
				
				
				<div class="row">
					<div class="four columns" style="padding-top:20px;">
					<% if (publicView) { %>
						<p>You're not signed in, so you can't contact or view this user's full detail!</p>
						<a href="login.jsp" class="small blue button">Login to see More</a><br/><br/>
					<% } else { %>
						<% 
							/* 
							* only render the friend button if this user is not already
							* friends with the visiting user
							*/
							int user1 = Math.min(visitingUser.getId(), user.getId());
							int user2 = Math.max(visitingUser.getId(), user.getId());
							Friend f = new Friend(user1, user2);
							f.fetch();
							System.out.println("----------> " + user1 + " to " + user2 + " has type " + f.getType());
							if(f.getType() != 3) {
								
						%>
							<% if((f.getType() == 2 && user2 == visitingUser.getId()) ||
									(f.getType() == 1 && user1 == visitingUser.getId())) {  %>
							<% } else { %>
								<a class="small green button friend-submit" id="<%= user.getId() %>">Add as Friend</a>
							<% } %>
							<!--  <form id="friend" action="MessageServlet" method="post">
								<input type="hidden" name="fromUser_id" value="<%= visitingUser.getId() %>" />
								<input type="hidden" name="toUser_id" value="<%= user.getId() %>" />
								<input type="hidden" name="type" value="friend" />
								<input class="small green button" type="submit" id="friend-submit" value="Friend Request" />  
							</form>
							 -->
						<% } %>
					<% } %>
					
						<!--
						////////////////////////////////////////////////////////////////////
						LINKS: 
						Sometimes clicking on buttons should logically take you to a 
						specific webpage to view a specific userid. 
						///////////////////////////////////////////////////////////////////
						
						Make sure that the servlet takes care of this by appending the proper
						id to whatever links necessitate them.
						 -->				
						
						
					</div>
					<a class="four columns gray box bluehover-highlight" style="width:175px; height:85px;"><%= activityManager.getActivityCountForUser(user.getId()) %><br/><br/><h6>Quizzes Taken</h6></a>
					<a class="four columns blue box hover-highlight" style="width:175px; height:85px;"><%= achievementManager.getByUserId(user.getId()).size() %><br/><br/><h6>Achievements Earned</h6></a>
					<a class="four columns pink box pinkhover-highlight" style="width:175px; height:85px;  float:left;"><%= roundTwoDecimals(activityManager.getAverageScoreUser(user.getId()) * 100) %>%<br/><br/><h6>Average Quiz Score</h6></a>
				</div>
				<br/>
				Tags:<% 
				TagManager tm = new TagManager();
				for(Tag tag:tm.fetchTagsForUser(userId)){ %>
						<a href="search.jsp?searchbar=<%=tag.getTag()%>" class="round tag" ><%="#"+tag.getTag()%></a>
						<% }%>
				<hr/>
				<br/>
				
				<% if(!publicView) { %>
				<h5>Performance Comparison:</h5><br/>
				
				<!-- RESULTS TABS -->
				<dl class="contained tabs">
				  <dd><a href="#past" class="active">Recent Activity</a></dd>
				  <dd><a href="#top">Quizzes Created</a></dd>
				</dl>
				
				<!-- RESULTS TAB CONTAINER -->
				<ul class="nice tabs-content contained">
			
					<!-- YOUR RECENT ACTIVITY -->
					<li id="pastTab" class="active">
				  		<br/>
						<table width="800" id="user-performance-table" class="tablesorter">
									<thead>
										<tr>
											<th width="15%">Quiz</th>
											<th width="15%">Score</th>
											<th width="15%">Time Taken</th>
											<th width="25%">Date</th>
										</tr>
									</thead>
									<tbody>
										<% for(Activity a : activities) { 
											Quiz q = Quiz.fetch(a.getQuiz_id());
											if(q != null) { %>
										<tr class="hover-highlight">
												<td><a href="quiz-summary.jsp?quiz_id=<%= q.getId()%>"><%= q.getName() %></a></td>
												<td><%= roundTwoDecimals(a.getScore() * (double)100) %>%</td>
												<td><%= roundTwoDecimals(a.getTimeTaken() / (double)1000) %> seconds</td>
												<td><%= a.getTimestamp() %></td>
										</tr>
										<% }
										}
										%>
									</tbody>
						</table>
				  	</li>
				  	
				  	<!-- YOUR QUIZZES CREATED -->
				  	<li id="topTab">
						<br/>
							<table width="900"  id="top-performance-table" class="tablesorter">
										<thead>
											<tr>
												<th width="15%">Quiz</th>
												<th width="30%">Description</th>
												<th width="5%">Category</th>
												<th width="15%">Time</th>
											</tr>
										</thead>
										<tbody>
											<% if(quizzes != null) {
												for(Quiz q : quizzes) {  
												Category category = new Category(q.getCategory_id()); %>
											<tr class="hover-highlight">
													<td><a href="quiz-summary.jsp?quiz_id=<%= q.getId()%>"><%= q.getName() %></a></td>
													<td><%= q.getDescription() %> seconds</td>
													<td><%= category.getName().equals("NULL") ? "Not specified" : category.getName() %></td>
													<td><%= q.getTimestamp().toString() %></td>
											</tr>
											<% }
											}%>
										</tbody>
							</table>
				  	</li>
				</ul>
				<% } %>
				
			</div>
		</div>
		<div id="footer" class="row">
			Stanford University Winter 2012. Built using Zurb Foundations and JQuery.
		</div>
		
	</div>
	<!-- container -->

	<!-- Included JS Files -->
	<script src="javascripts/jquery.min.js"></script>
	<script src="javascripts/modernizr.foundation.js"></script>
	<script src="javascripts/foundation.js"></script>
	<script src="javascripts/app.js"></script>
	<script src="javascripts/main.js"></script>
	<script src="javascripts/core.js"></script>
	<script src="javascripts/drag-n-drop.js"></script>
	<script src="javascripts/quiz.js"></script>
	<script src="javascripts/jquery.tablesorter.min.js"></script>
	
</body>
</html>
