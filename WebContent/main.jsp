<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*, com.backend.*, com.models.*, java.text.DecimalFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%!
	double roundTwoDecimals(double d) {
	    DecimalFormat twoDForm = new DecimalFormat("#.#");
		return Double.valueOf(twoDForm.format(d));
	}
%>
<%
//	session = request.getSession();
	
	/* special error checking to see if request and session are valid */
	if(session.getAttribute("special") == null) {	
		response.sendRedirect("login.jsp");
		return;
	}
	else if(!session.getAttribute("special").equals("29dd2f9f8d9312235caab2629e28ad45")) {
		response.sendRedirect("login.jsp");
		return;
	}

	/* Get the managers */
	ServletContext context = request.getServletContext();
	UserManager userManager = (UserManager) context.getAttribute("userManager");
	User user = (User) session.getAttribute("user");

	if(user == null) {
		response.sendRedirect("login.jsp");
		return;
	}
	
	
	if(user.isAuthenticated() == false) {
		session.setAttribute("special", "");
	    response.sendRedirect("login.jsp");
	    return;
	}
	
	boolean admin = user.isAdmin();
	
 	AnnouncementManager announcementManager = (AnnouncementManager) context.getAttribute("announcementManager");
 	QuizManager quizManager = (QuizManager) context.getAttribute("quizManager");
 	MessageManager messageManager = (MessageManager) context.getAttribute("messageManager");
 	ActivityManager activityManager = (ActivityManager) context.getAttribute("activityManager");
 	AchievementManager achievementManager = (AchievementManager) context.getAttribute("achievementManager");
	
// 	/* Get Annoucements */
 	List<Announcement> announcementArray = announcementManager.getAllAnnouncement();
	
// 	/* Get Best performance for this user (highest scoring activities) */
 	List<Activity> activities = activityManager.getRecentActivity(user.getId());

// 	/* Get friends activities */
 	List<Activity> friendActivities = activityManager.getRecentFriendActivity(user.getId(), userManager.getFriends(user.getId()));

// 	/* Get messages for this user */
	List<Message> messageArray = messageManager.getUserMessages(user.getId());

// 	/* get achievements */
 	List<Achievement> achievements = achievementManager.getByUserId(user.getId());

// 	/* Get recently created quizzes for the whole site */
	List<Quiz> recentQuizzes = quizManager.getRecent();
			
// 	/* Get popular quizzes for whole site */
	List<Quiz> popularQuizzes = quizManager.getPopular();
 	//List<Quiz> popularQuizzes = new ArrayList<Quiz>(); // tmp until QuizManager getPopular works
//  */
%>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Welcome to Foundation</title>
  
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="stylesheets/app.css">

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="stylesheets/ie.css">
	<![endif]-->


	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>
	<ul class="topbar" style="width:100%; height:25px;">
		<li><a href="main.jsp">Home</a></li>
		<li><a href="search.jsp">Search</a></li>
		<li><a href="LogoutServlet">Logout</a></li>
	</ul>
	
	<!-- container -->
	<div class="container">

		<div id="compose-Modal" class="reveal-modal">
			<h5>Send A Note</h5>
			<form id="compose" action="MessageServlet" method="post">
				<h6>Send to user:</h6>
				<input type="hidden" name="compose" value="1" />
				<input type="hidden" name="shouldRedirect" value="1" /> 
				<input type="text" name="username" placeholder="Username" />
				<h6>Message:</h6>
				<textarea class="expand" placeholder="Your Message" id="_message" name="message" rows="3"></textarea>
				<input type="hidden" name="type" value="note" />
				<input class="small green button" type="submit" value="Send" />  
			</form>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		
		<!-- Heading -->
		<div class="row" style="padding-top:30px">
			<div class="twelve columns">
				<div class="six columns">
					<div><img src="images/quizzically-logo-small.png" /></div>
					<p>Powered by Team Mach-One, cs108, Stanford University</p>
					<Form action="search.jsp" method="get">
						<input class="round" id="searchbar" type="search" size="50" name="searchbar" placeholder="Search for friends, quizzes, and more" />
					</Form> 
				</div>
			</div>
		<hr />
		</div>
		<!-- End Heading -->
		
	
		<div class="row">
			<div class="twelve columns">
			<!-- TABS: SHOULD NOT HAVE TO CHANGE -->
			<div class="three columns" style="height:100%; border: solid 0 #ddd; border-width: 0px 1px 0px 0px;">
				<dl class="nice vertical tabs">
				  <dd><a href="#nice0" class="active">Home</a></dd>
				  <dd><a href="#nice2">Popular</a></dd>
				  <dd><a href="#nice3">Create</a></dd>
				  <dd><a href="#nice4">Activity</a></dd>
				  <dd><a href="#nice5">Friends</a></dd>
				  <dd><a href="#nice6">Messages</a></dd>
				  <dd><a href="#nice7" >Achievements</a></dd>
				  <% if(admin == true) {
				  %> <dd><a href="admin.jsp" style="background-color:#217CDB; color:#FFF;" class="hover-highlight">Admin Page</a></dd> <%
					} %>
				</dl>
			</div>
			
			<div class="nine columns content" style="">
			<!-- TAB CONTENT: MODIFY AS NEEDED --->
			<ul class="nice tabs-content contained">
			  <!-- ANNOUNCEMENTS -->
			  <li class="active" id="nice0Tab">
			  	<div class="row">
					<div class="twelve columns show-panel">
						<h3>Announcement</h3>
						<br/>
						<div>
							<ul id="announcement-container" style="width:75%;">
								
								<%
								Announcement.Importance localAnnouncementImportance;
								String localAnnouncement;
								if(announcementArray.isEmpty() == false) 
								{
									for (int i = 0; i < announcementArray.size(); i++) 
									{
										Announcement announcement = announcementArray.get(i);
										localAnnouncement = announcement.getText();
										localAnnouncementImportance = announcement.getImportance(); %>
										<li class="announcement">
											<div class="alert-box <%=localAnnouncementImportance%>"><%=localAnnouncement%></div>
										</li>
								<% 
									} 
								} else {
									localAnnouncement = "No announcements!";
									localAnnouncementImportance = Announcement.Importance.LOW;
								%>
									<%= localAnnouncement %>
								<% } %>
							</ul>
						</div>
					</div>
				</div>
			  </li>
			  <!-- END ANNOUNCEMENTS -->
			  
			  
			  <!-- POPULAR QUIZZES -->
			  <li id="nice2Tab">
			  			<h3>Popular</h3>
			  			<br/>
						<table width="800">
							<thead>
								<tr>
									<th width="15%"># of Times Taken</th>
									<th width="30%">Name</th>
									<th width="15%">Questions</th>
									<th>Description</th>
								</tr>
							</thead>
							<tbody>
								<% 
								if(popularQuizzes.isEmpty() == false) {
									for(Quiz q: popularQuizzes) { %>
								<tr>
									<td><%= activityManager.getQuizCountTaken(q.getId()) %></td>
									<td class="hover-highlight">
										<a href="quiz-summary.jsp?quiz_id=<%= q.getId() %>">
											<%= q.getName() %>
										</a>
									</td>
									<td><%= q.getQuestions().size() %></td>
									<td><%= q.getDescription() %></td>
								</tr>
								<% 
									} 
								} 
								%>
							</tbody>
						</table>
			  
			  </li>
			  <!-- END POPULAR QUIZZES -->
			  
			  <!-- RECENTLY CREATED QUIZZES -->
			  <li id="nice3Tab">
			  
				 <div class="row">
					<h5>
						<a href="quiz-create.jsp">
							
							<div class="small button">
								Create a brand new quiz
							</div>
						</a>
					</h5>
					<br/>
						<h3>Recently Created</h3>
						
							<br/>
							<table width="750">
								<thead>
									<tr>
										<th width="20%">Name</th>
										<th width="15%">Author</th>
										<th width="40%">Description</th>
										<th width="5%">Category</th>
										<th width="25%">Time</th>
									</tr>
								</thead>
								<tbody>
									<%
										if (recentQuizzes.isEmpty() == false) {
											for (Quiz q: recentQuizzes) { 
												User creator = new User(q.getCreator_id()); 
												Category category = new Category(q.getCategory_id()); %>
											<tr>
												<td><a href="quiz-summary.jsp?quiz_id=<%=q.getId()%>"><%= q.getName() %></a></td>
												<td><a href="user.jsp?user_id=<%=creator.getId()%>"><%= creator.getName() %></a></td>
												<td><%= q.getDescription() %></td>
												<td><%= category.getName().equals("NULL") ? "Not specified" : category.getName() %></td>
												<td><%= q.getTimestamp() %></td>
											</tr>
									<% 		} 
										} 
									%>
								</tbody>
							</table>
						
					</div>
			  </li>
			  <!-- END RECENTLY CREATED QUIZZES -->
			  
			  
			  <!-- USER'S RECENT ACTIVITY -->
			  <li id="nice4Tab">
				<div class="row">
					<h3>My Recent Activity</h3>
							<br/>
							<table width="800">
								<thead>
									<tr>
										<th width="15%">Quiz Name</th>
										<th width="15%">Score</th>
										<th width="15%">Time Elapsed</th>
										<th width="15%">Recorded</th>
									</tr>
								</thead>
								<tbody>
									<% if(activities != null) {
										for (Activity a: activities) { 
										Quiz quiz = Quiz.fetch(a.getQuiz_id());
										if(quiz == null) continue;
									%>
									<tr>
										<td><a href="quiz-summary.jsp?quiz_id=<%=quiz.getId()%>"><%= quiz.getName() %></a></td>
										<td><%= roundTwoDecimals(a.getScore() * (double)100) %>%</td>
										<td><%= roundTwoDecimals(a.getTimeTaken() / (double)1000) %> sec</td>
										<td><%= a.getTimestamp() %></td>
									</tr>
									<% } 
									} %>
								</tbody>
							</table>
					</div>
				</li>
			  <!-- END USER'S RECENT ACTIVITY -->
			
			
			  <!-- FRIENDS' RECENT ACTIVITY -->
			  <li id="nice5Tab">
			  	<div class="row">
					<h3>Friends Activity</h3>
							<br/>
							<table width="800">
								<thead>
									<tr>
										<th width="15%">Friend</th>
										<th width="10%">Quiz Name</th>
										<th width="10%">Score</th>
										<th width="15%">Time Elapsed</th>
										<th>Recorded</th>
									</tr>
								</thead>
								<tbody>
									<% for (Activity a: friendActivities) { 
										Quiz quiz = Quiz.fetch(a.getQuiz_id());
										if(quiz == null) continue;
									%>
									<tr>
										<td><a href="user.jsp?user_id=<%= a.getUser_id() %>"><%= a.getUsername() %></a></td>
										<td><a href="quiz-summary.jsp?quiz_id=<%= quiz.getId() %>"><%= quiz.getName() %></td>
										<td><%= roundTwoDecimals(a.getScore() * (double)100) %>%</td>
										<td><%= roundTwoDecimals(a.getTimeTaken() / (double)1000) %> sec</td>
										<td><%= a.getTimestamp() %></td>
									</tr>
									<% } %>
								</tbody>
							</table>
					</div>
			  </li>
			   <!-- END FRIENDS' RECENT ACTIVITY -->
			  
			   <!-- MESSAGES -->
			  <li id="nice6Tab">
			  	<a class="small red button" data-reveal-id="compose-Modal">Compose</a>
			  	<br/><br/>
				<table width="800">
							<thead>
								<tr>
									<th width="15%">From</th>
									<th width="25%">Message</th>
									<th width="30%">Time</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<% 
							if(messageArray.isEmpty() == false) {
								for (int i = 0; i < messageArray.size(); i++) {
									Message message = messageArray.get(i);
									if(message.getMessageType() == null) continue;
									if(message.getMessageType().equals(Message.types[2])) {
										/*
										* Dont get the friend type for a user who's 
										*/
										Friend f = new Friend(message.getFromUserId(), message.getToUserId());
										f.fetch();
										if(f.getType() == 3) 
											continue;
									}
									User fromUser = new User(message.getFromUserId());
							%>
								<tr>
									<td class="hover-highlight"><a href="user.jsp?user_id=<%=message.getFromUserId()%>"><%= fromUser.getName() %></a></td>
									<td><% if(message.getMessageType().equals(Message.types[0])) { %>
											<a href="quiz-summary.jsp?quiz_id=<%= message.getQuiz_id() %>">
										<% } %>
										<%= message.getMessage() %>
										<% if(message.getMessageType().equals(Message.types[0])) { %>
											</a>
										<% } %> 
									</td>
									<td><%=message.getTimestamp()%></td>
									<!-- Actions -->
									<td id="<%= message.getId() %>">
									<% if(message.getMessageType().equals(Message.types[0])) { %>
									
									<% } else if(message.getMessageType().equals(Message.types[1]) && !message.isRead()) {  %>
										<a class="read-message">Mark as Read</a>
									<% } else if(message.getMessageType().equals(Message.types[2])) {
										int user1 = Math.min(message.getFromUserId(), message.getToUserId());
										int user2 = Math.max(message.getFromUserId(), message.getToUserId());
										Friend f = new Friend(user1, user2);
										f.fetch();
										if(f.getType() != 3) {
									%>
										<a id="<%= message.getFromUserId() %>" class="confirm-friendship">Confirm</a><br/>
									<% } 
									} else { %>
										
									<% } %>
									</td>
									
								</tr>
							<% 
								} }
							%>
							</tbody>
				</table>
						
				</li>
			  <!-- FRIENDS' RECENT ACTIVITY -->
			
			  <!-- ACHIEVEMENTS -->
			  <li id="nice7Tab">
			  <div class="row">
					<h3>Achievements</h3>
							<br/>
							<table width="760">
								<thead>
									<tr>
										<th width="15%"></th>
										<th width="20%">Award</th>
										<th width="30%">Description</th>
										<th width="25%">Date</th>
									</tr>
								</thead>
								<tbody>
									<% for (Achievement a: achievements) { %>
									<tr>
										<td><img src="<%= a.getUrl() %>" width="48" height="48" /></td>
										<td><%= a.getAward() %></td>
										<td><%= a.getDescription() %></td>
										<td><%= a.getTimestamp().toString() %></td>
									</tr>
									<% } %>
								</tbody>
							</table>
						
					</div>
			  </li>
			 <!-- END ACHIEVEMENTS -->
			 
			</ul>  <!-- END OF TAB CONTENT -->
			</div> <!--  close row --> 
			</div>
		</div>

		<div id="footer" class="row">
			Stanford University Winter 2012. Site powered by Google App Engine, built using Zurb Foundations, and JQuery.
		</div>
		
	</div> <!-- close container -->




	<!-- Included JS Files -->
	<script src="javascripts/jquery.min.js"></script>
	<script src="javascripts/modernizr.foundation.js"></script>
	<script src="javascripts/foundation.js"></script>
	<script src="javascripts/app.js"></script>
	<script src="javascripts/main.js"></script>

</body>
</html>
